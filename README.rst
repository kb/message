message
*******

Django message/chat application

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-message
  source venv-message/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x


Usage
=====

Settings
--------

`Message`.`message_type`s which require a MessageAction to be automatically
created for newly saved messages for the recipient of `message_to` or when
@mentioned + the required Action Name::

  AUTO_ACTION_MESSAGE_TYPES = {"Private": "Replied"}

For each message serialised, we list all the `MessageAction`s `actioned_by` the
user requesting the list AND optionally those `actioned_for` them by another
user if they are of a certain type.::

  INCLUDE_ACTION_TYPES_WHEN_FOR = ("Replied", "Liked")

Action types which the user should be notified about.::

  NOTIFIABLE_ACTIONS = ("Liked", "Replied",)

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
