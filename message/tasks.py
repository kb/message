# -*- encoding: utf-8 -*-
import logging

from celery import task

from .search import MessageIndex
from search.search import SearchIndex


logger = logging.getLogger(__name__)


@task()
def rebuild_message_index():
    logger.info("Drop and rebuild message index...")
    index = SearchIndex(MessageIndex())
    count = index.rebuild()
    logger.info("Message index rebuilt - {} records - complete".format(count))
    return count


@task()
def refresh_message_index():
    logger.info("Refresh message index...")
    index = SearchIndex(MessageIndex())
    count = index.refresh()
    logger.info("Message index refreshed - {} records - complete".format(count))
    return count


@task()
def update_message_index(pk):
    logger.info("Search index - update message {}".format(pk))
    index = SearchIndex(MessageIndex())
    count = index.update(pk)
    logger.info(
        "Search index - updated message {} ({} record)".format(pk, count)
    )
    return count
