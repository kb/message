# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from .views import (
    ConversationsListView,
    LikeConversationView,
    MessageDetailView,
    NewConversationView,
    ReportedDetailView,
    ReportedListView,
    ReportView,
    ReportedOveruledView,
    ReportedPostponeView,
    ReportedSustainedView,
    UnLikeConversationView,
)


urlpatterns = [
    url(
        regex=r"^conversations/(?P<pk>\d+)/$",
        view=ConversationsListView.as_view(),
        name="conversation.list",
    ),
    url(
        regex=r"^conversations/$",
        view=ConversationsListView.as_view(),
        name="conversation.list",
    ),
    url(
        regex=r"^conversations/new/$",
        view=NewConversationView.as_view(),
        name="conversations.new",
    ),
    url(
        regex=r"^message/(?P<pk>\d+)/$",
        view=MessageDetailView.as_view(),
        name="message.detail",
    ),
    url(
        regex=r"^like/(?P<pk>\d+)/$",
        view=LikeConversationView.as_view(),
        name="conversation.message.like",
    ),
    url(
        regex=r"^unlike/(?P<pk>\d+)/$",
        view=UnLikeConversationView.as_view(),
        name="conversation.message.unlike",
    ),
    url(
        regex=r"^reported/$",
        view=ReportedListView.as_view(),
        name="reported.list",
    ),
    url(
        regex=r"^reported/detail/(?P<pk>\d+)/$",
        view=ReportedDetailView.as_view(),
        name="reported.detail",
    ),
    url(
        regex=r"^report/(?P<pk>\d+)/$",
        view=ReportView.as_view(),
        name="reported.report",
    ),
    url(
        regex=r"^overrule/(?P<pk>\d+)/$",
        view=ReportedOveruledView.as_view(),
        name="reported.overruled",
    ),
    url(
        regex=r"^sustain/(?P<pk>\d+)/$",
        view=ReportedSustainedView.as_view(),
        name="reported.sustained",
    ),
    url(
        regex=r"^postpone/(?P<pk>\d+)/$",
        view=ReportedPostponeView.as_view(),
        name="reported.postpone",
    ),
]
