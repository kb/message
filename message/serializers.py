# -*- encoding: utf-8 -*-
import logging

from django.conf import settings
from django.db import transaction
from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault
from contact.models import Contact
from contact.serializers import ContactSerializer
from message.models import Message, MessageAction
from .tasks import update_message_index

logger = logging.getLogger(__name__)

AUTO_ACTION_MESSAGE_TYPES = settings.AUTO_ACTION_MESSAGE_TYPES
INCLUDE_ACTION_TYPES_WHEN_FOR = settings.INCLUDE_ACTION_TYPES_WHEN_FOR


class ContactActionsMixin:
    def _get_context_contact_message_actions(self, message):
        """Return prefetched MessageActions per message for the user in context.
        """
        # Who is active?
        active_contact_id = 0
        try:
            # Check that the account is not being curated.
            active_contact_id = self.context["request"].query_params.get(
                "curid", 0
            )
        except AttributeError:
            # We're catching the error which occurs when this serializer is
            # accessed from inside a standard Django View, and not a ViewSet.
            pass
        if int(active_contact_id):
            active_contact_id = int(active_contact_id)
        # Get all the actions for this message.
        message_actions = message.messageaction_set.all()
        # Filter the actions to the current user
        if active_contact_id:
            # Get all the actions when *BY* the curated account
            mactions = [
                ma
                for ma in message_actions
                if ma.action_by.id == active_contact_id
            ]
            # Get the preset actions when *FOR* the curated account
            mactions += [
                ma
                for ma in message_actions
                if ma.action_for.id == active_contact_id
                and ma.action_name in INCLUDE_ACTION_TYPES_WHEN_FOR
            ]
        else:
            # Or... Get all the actions when *BY* request user.
            mactions = [
                ma
                for ma in message_actions
                if ma.action_by.user.id == self.context["request"].user.id
            ]
            # Get the preset actions when *FOR* the curated account
            mactions += [
                ma
                for ma in message_actions
                if ma.action_for.user.id == self.context["request"].user.id
                and ma.action_name in INCLUDE_ACTION_TYPES_WHEN_FOR
            ]
        return mactions

    def get_actions_by(self, message):
        """Get only message actions by the requesting contact."""
        mactions = self._get_context_contact_message_actions(message)
        serializer = MessageActionSerializer(instance=mactions, many=True)
        return serializer.data


class LastOfEachSerializer(ContactActionsMixin, serializers.ModelSerializer):

    picture = serializers.CharField(allow_blank=True, required=False)
    actions_by = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = (
            "id",
            "actions_by",
            "created",
            "message",
            "message_from",
            "message_style",
            "message_to",
            "message_type",
            "picture",
        )
        readonly = ["actions_by"]  # "action_summary",


class MessageSerializer(ContactActionsMixin, serializers.ModelSerializer):

    picture = serializers.CharField(allow_blank=True, required=False)
    # action_summary = serializers.SerializerMethodField()
    actions_by = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = (
            "id",
            "action_summary",
            "actions_by",
            "created",
            "message",
            "message_from",
            "message_style",
            "message_to",
            "message_type",
            "picture",
        )
        readonly = ["actions_by", "action_summary"]

    @staticmethod
    def setup_eager_loading(queryset):
        """ Perform necessary eager loading of data.

        Usage::

            class LoveLettersViewSet(viewsets.ModelViewSet):

                authentication_classes = (TokenAuthentication,)
                permission_classes = (IsAuthenticated,)
                serializer_class = MessageSerializer

                def get_queryset(self):
                    loveletters = Message.objects.filter(
                        message_type="LoveLetters"
                    )
                    loveletters = MessageSerializer.setup_eager_loading(
                        loveletters
                    )
                    return loveletters
         """
        queryset = queryset.select_related(
            "message_from",
            "message_from__gender",
            "message_from__user",
            "message_to",
            "message_to__gender",
            "message_to__user",
        ).prefetch_related(
            "messageaction_set",
            "messageaction_set__action_by",
            "messageaction_set__action_by__gender",
            "messageaction_set__action_by__user",
            "messageaction_set__action_for",
            "messageaction_set__action_for__gender",
            "messageaction_set__action_for__user",
            "messageaction_set__message",
            "messageaction_set__message__message_to",
            "messageaction_set__message__message_to__gender",
            "messageaction_set__message__message_to__user",
            "messageaction_set__message__message_from",
            "messageaction_set__message__message_from__gender",
            "messageaction_set__message__message_from__user",
        )
        return queryset

    def _parse_mentions(self, instance):
        for mention in instance.get_mentions("tributes"):
            try:
                contact_mentioned = Contact.objects.get(id=mention)
                MessageAction.objects.add_message_action(
                    instance,
                    instance.message_from,
                    contact_mentioned,
                    AUTO_ACTION_MESSAGE_TYPES[instance.message_type],
                )
            except self.model.DoesNotExist:
                pass  # Maybe deleted?

    def create(self, validated_data):
        picture_url = None
        if validated_data.get("picture", None):
            picture_url = validated_data.pop("picture")
        instance = None
        with transaction.atomic():
            instance = Message.objects.create_message(**validated_data)
            instance.picture = picture_url
            instance.save()
            # Add actions which show this message unviewed by target user.
            if instance.message_type in AUTO_ACTION_MESSAGE_TYPES.keys():
                # Add action for the user referenced in the message_to field.
                if instance.message_to:
                    v = MessageAction.objects.add_message_action(
                        instance,
                        instance.message_from,
                        instance.message_to,
                        AUTO_ACTION_MESSAGE_TYPES[instance.message_type],
                    )
                # Look for users @mentioned in the message body.
                self._parse_mentions(instance)
            transaction.on_commit(
                lambda: update_message_index.delay(instance.pk)
            )
        return instance

    def update(self, instance, validated_data):
        picture_url = None
        if validated_data.get("picture", None):
            picture_url = validated_data.pop("picture")
        with transaction.atomic():
            instance = super().update(instance, validated_data)
            instance.picture = picture_url
            instance.save()
            self._parse_mentions(instance)
            transaction.on_commit(
                lambda: update_message_index.delay(instance.pk)
            )
        return instance

    def get_action_summary(self, message):
        """Get a summary of actions for each message."""
        mactions = message.messageaction_set.all()
        # Run a count of each action for this message. Using a dict:
        maction_count = {}
        for maction in mactions:
            # Accrue the total for each action type as dict key.
            maction_count[maction.action_name] = (
                maction_count.get(maction.action_name, 0) + 1
            )
        # Convert to a list of message_actions each with a total.
        maction_summary = []
        for key in maction_count:
            maction_summary.append(
                {"action_name": key, "total": maction_count[key]}
            )
        return sorted(maction_summary, key=lambda k: k["action_name"])


class MessageActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageAction
        fields = (
            "id",
            "action_by",
            "action_for",
            "message",
            "action_name",
            "dismissed",
        )

    # def create(self, validated_data):
    #     maction = None
    #     action_by = validated_data.get("action_by", None)
    #     action_for = validated_data.get("action_for", None)
    #     action_name = validated_data.get("action_name", None)
    #     # "Added" action does not need a message. Allow for none.
    #     message = validated_data.get("message", None)
    #     print(
    #         "MessageActionSerializer", action_by, action_for, action_name
    #     )
    #     if action_name:
    #         with transaction.atomic():
    #             # Record the action once only
    #             maction = MessageAction.objects.add_message_action(
    #                 message, action_by, action_for, action_name
    #             )
    #     return maction


class MessageActionSummarySerializer(serializers.Serializer):
    class Meta:
        fields = ("action_name", "total")


class ViewMessageSerializer(MessageSerializer):

    message_from = ContactSerializer()
    message_to = ContactSerializer()
    created = serializers.DateTimeField(format="%d/%m/%Y %H:%m")
    was_reported = serializers.SerializerMethodField()
    was_liked = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = (
            "id",
            "action_summary",
            "actions_by",
            "created",
            "message",
            "message_from",
            "message_style",
            "message_to",
            "message_type",
            "picture",
            "was_reported",
            "was_liked",
        )
        readonly = ["action_summary", "actions_by", "was_reported", "was_liked"]

    def get_was_liked(self, message):
        """Get whether the requesting contact already liked this message."""
        # HINT: Use as template for affirming other actions for this contact
        mactions = self._get_context_contact_message_actions(message)
        for ma in mactions:
            if ma.action_name == MessageAction.LIKED:
                return bool(ma.pk)
        return False

    def get_was_reported(self, message):
        """Get whether the requesting contact already liked this message."""
        # HINT: Use as template for affirming other actions for this contact
        mactions = self._get_context_contact_message_actions(message)
        for ma in mactions:
            if ma.action_name == MessageAction.REPORT:
                return bool(ma.pk)
        return False
