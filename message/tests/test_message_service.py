# -*- encoding: utf-8 -*-
import json
import pytest

from dateutil.relativedelta import relativedelta
from django.utils import timezone
from contact.models import Contact, ContactAddress
from contact.tests.factories import ContactAddressFactory, ContactFactory
from login.tests.factories import UserFactory
from message.models import Message, MessageAction
from message.search import MessageIndex, MessageIndexMixin, Row
from message.service import (
    search_anyones_of_type,
    search_belonging_to,
    search_between_two_contacts,
    search_directed_to,
    search_lastofeach_per_contact,
)
from message.tests.factories import MessageFactory, MessageActionFactory
from search.models import SearchFormat
from search.search import SearchIndex


PRIV = Message.MESSAGE_TYPE_PRIVATE
PUBL = Message.MESSAGE_TYPE_PUBLIC
TRIB = "trib"
WALL = "wall"
NO_CRITERIA = ""


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_service_search_anyones_of_type():
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message_type=PRIV)
    m2 = MessageFactory(message_from=c2, message_to=c1, message_type=TRIB)
    m3 = MessageFactory(message_from=c1, message_to=c2, message_type=TRIB)
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 3 == search_index.update()
    # Search for TRIB messages.
    result = search_anyones_of_type(NO_CRITERIA, TRIB)
    assert 2 == result.count()
    assert [m2.pk, m3.pk] == [x.pk for x in result]
    # Search for PRIV messages.
    result = search_anyones_of_type(NO_CRITERIA, PRIV)
    assert 1 == result.count()
    assert [m1.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_service_search_between_two_contacts_with_type():
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    # Private by default.
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    # c4 messages should not be included in the results.
    m7 = MessageFactory(message_from=c1, message_to=c4, message="c1 c4")
    m8 = MessageFactory(message_from=c2, message_to=c4, message="c2 c4")
    m9 = MessageFactory(message_from=c3, message_to=c4, message="c3 c4")
    # TRIB messages should not be included.
    MessageFactory(message_from=c1, message_to=c2, message_type=TRIB)
    MessageFactory(message_from=c2, message_to=c1, message_type=TRIB)
    MessageFactory(message_from=c1, message_to=c2, message_type=TRIB)
    MessageFactory(message_from=c1, message_to=c3, message_type=TRIB)
    MessageFactory(message_from=c3, message_to=c1, message_type=TRIB)
    MessageFactory(message_from=c2, message_to=c3, message_type=TRIB)
    # WALL messages should not be included in the results.
    MessageFactory(message_from=c1, message_type=WALL)
    MessageFactory(message_from=c2, message_type=WALL)
    MessageFactory(message_from=c3, message_type=WALL)
    # Only include PRIV messages
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 18 == search_index.update()
    # Tests are the same as `test_message_service_search_messages_between_two_users`
    # except we've added the PRIV type + plus some wildcards.
    # Search for messages between c1 and c2.
    result = search_between_two_contacts(NO_CRITERIA, c1.pk, c2.pk, PRIV)
    assert 3 == result.count()
    assert [m1.pk, m2.pk, m3.pk] == [x.pk for x in result]
    # Search for messages between c1 and c3.
    result = search_between_two_contacts(NO_CRITERIA, c1.pk, c3.pk, PRIV)
    assert 2 == result.count()
    assert [m4.pk, m5.pk] == [x.pk for x in result]
    # Search for messages between c2 and c3.
    result = search_between_two_contacts(NO_CRITERIA, c2.pk, c3.pk, PRIV)
    assert 1 == result.count()
    assert [m6.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_service_search_belonging_to():
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 6 == search_index.update()
    # Search for c1's messages.
    result = search_belonging_to(NO_CRITERIA, c1.pk, PRIV)
    assert 5 == result.count()
    assert [m1.pk, m2.pk, m3.pk, m4.pk, m5.pk] == [x.pk for x in result]
    # Search for c2's messages.
    result = search_belonging_to(NO_CRITERIA, c2.pk, PRIV)
    assert 4 == result.count()
    assert [m1.pk, m2.pk, m3.pk, m6.pk] == [x.pk for x in result]
    # Search for c3's messages.
    result = search_belonging_to(NO_CRITERIA, c3.pk, PRIV)
    assert 3 == result.count()
    assert [m4.pk, m5.pk, m6.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_service_search_between_two_contacts():
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    # c4 messages should not be included in the results.
    m7 = MessageFactory(message_from=c1, message_to=c4, message="c1 c4")
    m8 = MessageFactory(message_from=c2, message_to=c4, message="c2 c4")
    m9 = MessageFactory(message_from=c3, message_to=c4, message="c3 c4")
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 9 == search_index.update()
    # Search for messages between c1 and c2.
    result = search_between_two_contacts(NO_CRITERIA, c1.pk, c2.pk, PRIV)
    assert 3 == result.count()
    assert [m1.pk, m2.pk, m3.pk] == [x.pk for x in result]
    # Search for messages between c1 and c3.
    result = search_between_two_contacts(NO_CRITERIA, c1.pk, c3.pk, PRIV)
    assert 2 == result.count()
    assert [m4.pk, m5.pk] == [x.pk for x in result]
    # Search for messages between c2 and c3.
    result = search_between_two_contacts(NO_CRITERIA, c2.pk, c3.pk, PRIV)
    assert 1 == result.count()
    assert [m6.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_service_search_between_two_contacts_strings():
    """String parameters dont matter."""
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    # c4 messages should not be included in the results.
    m7 = MessageFactory(message_from=c1, message_to=c4, message="c1 c4")
    m8 = MessageFactory(message_from=c2, message_to=c4, message="c2 c4")
    m9 = MessageFactory(message_from=c3, message_to=c4, message="c3 c4")
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 9 == search_index.update()
    # Search for messages between c1 and c2.
    result = search_between_two_contacts(
        NO_CRITERIA, str(c1.pk), str(c2.pk), PRIV
    )
    assert 3 == result.count()
    assert [m1.pk, m2.pk, m3.pk] == [x.pk for x in result]
    # Search for messages between c1 and c3.
    result = search_between_two_contacts(
        NO_CRITERIA, str(c1.pk), str(c3.pk), PRIV
    )
    assert 2 == result.count()
    assert [m4.pk, m5.pk] == [x.pk for x in result]
    # Search for messages between c2 and c3.
    result = search_between_two_contacts(
        NO_CRITERIA, str(c2.pk), str(c3.pk), PRIV
    )
    assert 1 == result.count()
    assert [m6.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_service_search_helper_messages_directed_to():
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    # c4 messages should not be included in the results.
    m7 = MessageFactory(message_from=c1, message_to=c4, message="c1 c4")
    m8 = MessageFactory(message_from=c2, message_to=c4, message="c2 c4")
    m9 = MessageFactory(message_from=c3, message_to=c4, message="c3 c4")
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 9 == search_index.update()
    # Search for messages directed at c1.
    result = search_directed_to(NO_CRITERIA, c1.pk, PRIV)
    assert 2 == result.count()
    assert [m2.pk, m5.pk] == [x.pk for x in result]
    # Search for messages directed at c2.
    result = search_directed_to(NO_CRITERIA, c2.pk, PRIV)
    assert 2 == result.count()
    assert [m1.pk, m3.pk] == [x.pk for x in result]
    # Search for messages directed at c3.
    result = search_directed_to(NO_CRITERIA, c3.pk, PRIV)
    assert 2 == result.count()
    assert [m4.pk, m6.pk] == [x.pk for x in result]
    # Search for messages directed at c4.
    result = search_directed_to(NO_CRITERIA, c4.pk, PRIV)
    assert 3 == result.count()
    assert [m7.pk, m8.pk, m9.pk] == [x.pk for x in result]
