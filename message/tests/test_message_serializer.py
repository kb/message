# -*- encoding: utf-8 -*-
import pytest

from django.test.client import RequestFactory
from django.test import TestCase
from message.models import Message
from message.serializers import MessageSerializer
from .factories import MessageActionFactory, MessageFactory


@pytest.mark.django_db
class MyTests(TestCase):
    def test_message_serializer_messageserializer_prefetch(self):
        """Test the prefetch."""
        m1 = MessageFactory()
        m2 = MessageFactory()
        MessageActionFactory(message=m1)
        MessageActionFactory(message=m1)
        MessageActionFactory(message=m2)
        MessageActionFactory(message=m2)
        factory = RequestFactory()
        request = factory.get("messages")
        request.user = m1.message_from.user
        qs = MessageSerializer.setup_eager_loading(Message.objects.current())
        serializer = MessageSerializer(
            context={"request": request}, instance=qs, many=True
        )
        with self.assertNumQueries(8):
            conversation = serializer.data
