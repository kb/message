# -*- encoding: utf-8 -*-
import pytest

from django.test import override_settings
from message.models import Message, MessageAction
from contact.tests.factories import ContactFactory
from .factories import MessageFactory, MessageActionFactory

PRIV = Message.MESSAGE_TYPE_PRIVATE
publ = Message.MESSAGE_TYPE_PUBLIC
trib = "Tribute"

LIKED = MessageAction.LIKED
REPORT = MessageAction.REPORT


@pytest.mark.django_db
def test_str():
    message = MessageFactory()
    str(message)


@pytest.mark.django_db
def test_message_models_create():
    c1 = ContactFactory()
    c2 = ContactFactory()
    validated_data = {
        "message_from": c1,
        "message_to": c2,
        "message": "Did this test work?",
        "picture": "/message/tests/animals_amphibian_frog.gif",
    }
    Message.objects.create_message(**validated_data)
    assert Message.objects.current().count() == 1
    # Data Integrity


@pytest.mark.django_db
def test_message_models_mentions():
    mentions1 = '<a href="/tributes/1">Person 1</a>'
    mentions2 = 'Thank you <a href="/walls/1">Person 1</a> and <a href="/walls/23">Person 23</a> '
    mentions3 = 'To <a href="/profile/page/1">Person 1</a> <a href="/profile/page/23">Person 23</a> <a href="/profile/page/456">Person 456</a>. You are mentioned.'
    m1 = MessageFactory(message=mentions1)
    m2 = MessageFactory(message=mentions2)
    m3 = MessageFactory(message=mentions3)
    assert [1] == m1.get_mentions("tributes")
    assert [1, 23] == m2.get_mentions("walls")
    assert [1, 23, 456] == m3.get_mentions("profile/page")
    mentions_duplicated = 'To <a href="/p/456">Person 456</a> <a href="/p/1">Person 1</a> <a href="/p/23">Person 23</a> and again <a href="/p/456">Person 456</a> and again <a href="/p/1">Person 1</a> and again <a href="/p/23">Person 23</a>. You are mentioned.'
    m4 = MessageFactory(message=mentions_duplicated)
    assert [1, 23, 456] == m4.get_mentions("p")


@pytest.mark.django_db
def test_message_models_conversations():
    # Using these shorthand names to stop black making the page too long
    # 4 users
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    # 5 messages between cXXc + c1 + 2 public which should not be factored
    set1 = [
        MessageFactory(message_from=cXXc, message_to=c1, message="a1"),
        MessageFactory(message_from=c1, message_to=cXXc, message="1b"),
        MessageFactory(message_from=cXXc, message_to=c1, message="c1"),
        MessageFactory(message_from=c1, message_to=cXXc, message="1d"),
        MessageFactory(message_from=cXXc, message_to=c1, message="1e"),
        MessageFactory(
            message_from=c1,
            message_to=cXXc,
            message="2A",
            message_type=Message.MESSAGE_TYPE_PUBLIC,
        ),
        MessageFactory(
            message_from=cXXc,
            message_to=c1,
            message="B2",
            message_type=Message.MESSAGE_TYPE_PUBLIC,
        ),
    ]
    # 6 messages between cXXc + c2 + 2 public which should not be factored
    set2 = [
        MessageFactory(message_from=cXXc, message_to=c2, message="a2"),
        MessageFactory(message_from=c2, message_to=cXXc, message="2b"),
        MessageFactory(message_from=cXXc, message_to=c2, message="c2"),
        MessageFactory(message_from=c2, message_to=cXXc, message="2d"),
        MessageFactory(message_from=c2, message_to=cXXc, message="e2"),
        MessageFactory(message_from=c2, message_to=cXXc, message="2f"),
        MessageFactory(
            message_from=c2,
            message_to=cXXc,
            message="2C",
            message_type=Message.MESSAGE_TYPE_PUBLIC,
        ),
        MessageFactory(
            message_from=cXXc,
            message_to=c2,
            message="D2",
            message_type=Message.MESSAGE_TYPE_PUBLIC,
        ),
    ]
    # 2 messages between c1 + c2
    set3 = [
        MessageFactory(message_from=c1, message_to=c2, message="12"),
        MessageFactory(message_from=c2, message_to=c1, message="21"),
        MessageFactory(
            message_from=c1,
            message_to=c2,
            message="12PUBLIC",
            message_type=Message.MESSAGE_TYPE_PUBLIC,
        ),
        MessageFactory(
            message_from=c2,
            message_to=c1,
            message="21PUBLIC",
            message_type=Message.MESSAGE_TYPE_PUBLIC,
        ),
    ]
    # It's the movie of the year!
    # Data Integrity (public messages not accounted)
    assert Message.objects.conversations(cXXc).count() == 2
    assert Message.objects.conversations(c3).count() == 0
    # Get all the messages in one conversation
    assert Message.objects.conversation(cXXc, c1).count() == 5
    assert Message.objects.conversation(cXXc, c2).count() == 6
    assert Message.objects.conversation(cXXc, c3).count() == 0
    # Get all the messages regardless of which message_id
    for sin1 in set1:
        assert Message.objects.message_conversation(cXXc, sin1.id).count() == 5
    for sin2 in set2:
        assert Message.objects.message_conversation(cXXc, sin2.id).count() == 6
    # No messages when no messages
    assert Message.objects.conversation(cXXc, c3).count() == 0
    assert Message.objects.conversation(c3, cXXc).count() == 0
    # Last private message in each set (at the top)
    convs = Message.objects.conversations(cXXc)
    assert ["2f", "1e"] == [c.message for c in convs]


@pytest.mark.django_db
@override_settings(NOTIFIABLE_ACTIONS=(LIKED,))
def test_message_models_message_actions():
    publ = Message.MESSAGE_TYPE_PUBLIC
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_type=publ)
    m2 = MessageFactory(message_from=c2, message_type=publ)
    MessageAction.objects.add_message_action(m1, c2, c1, LIKED)
    MessageAction.objects.add_message_action(m1, c3, c1, LIKED)
    MessageAction.objects.add_message_action(m1, c4, c1, LIKED)
    # m1 has three likes
    assert m1.actions_of_type(LIKED).count() == 3
    # m2 has no likes
    assert m2.actions_of_type(LIKED).count() == 0
    # c1 did not LIKED m1
    assert not m2.was_actioned_by(c1, LIKED)
    # c2 did not LIKED m2
    assert not m2.was_actioned_by(c2, LIKED)
    # c2 liked m1
    assert m1.was_actioned_by(c2, LIKED)
    # Have c2 unlike m1
    MessageAction.objects.del_message_action(m1, c2, c1, LIKED)
    # m1 has two likes now
    assert m1.actions_of_type(LIKED).count() == 2
    # c2 no longer likes m1
    assert not m1.was_actioned_by(c2, LIKED)
    MessageAction.objects.add_message_action(m2, c4, c2, LIKED)
    # Return all c2's m2 actions
    assert m2.actions_by(c4).count() == 1
    # Can't keep adding the same action for the same message
    MessageAction.objects.add_message_action(m2, c4, c2, LIKED)
    MessageAction.objects.add_message_action(m2, c4, c2, LIKED)
    MessageAction.objects.add_message_action(m2, c4, c2, LIKED)
    MessageAction.objects.add_message_action(m2, c4, c2, LIKED)
    assert m2.actions_by(c4).count() == 1


@pytest.mark.django_db
@override_settings(NOTIFIABLE_ACTIONS=(LIKED, REPORT))
def test_message_models_message_actions_summary():
    publ = Message.MESSAGE_TYPE_PUBLIC
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_type=publ)
    MessageAction.objects.add_message_action(m1, c2, c1, LIKED)
    MessageAction.objects.add_message_action(m1, c3, c1, LIKED)
    MessageAction.objects.add_message_action(m1, c4, c1, LIKED)
    MessageAction.objects.add_message_action(m1, c2, c1, REPORT)
    MessageAction.objects.add_message_action(m1, c3, c1, REPORT)
    assert m1.action_summary().count() == 2
    assert m1.action_summary()[0]["action_name"] == LIKED
    assert m1.action_summary()[0]["total"] == 3
    assert m1.action_summary()[1]["action_name"] == REPORT
    assert m1.action_summary()[1]["total"] == 2


@pytest.mark.django_db
@override_settings(
    NOTIFIABLE_ACTIONS=("action1", "action2", "action3", "action4")
)
def test_message_models_messageactions_per_contact_list():
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory()
    m2 = MessageFactory()
    c1ma1 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action1"
    )
    c1ma2 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action2"
    )
    c1ma3 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action3"
    )
    c1ma4 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action4"
    )
    c2ma5 = MessageActionFactory(
        message=m1, action_for=c2, action_name="action1"
    )
    c2ma6 = MessageActionFactory(
        message=m1, action_for=c2, action_name="action2"
    )
    c1ma7 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action1"
    )
    c1ma8 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action2"
    )
    c1ma9 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action3"
    )
    c1ma10 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action4"
    )
    c2ma11 = MessageActionFactory(
        message=m2, action_for=c2, action_name="action1"
    )
    c2ma12 = MessageActionFactory(
        message=m2, action_for=c2, action_name="action2"
    )
    assert 12 == MessageAction.objects.all().count()
    assert 8 == MessageAction.objects.current().filter(action_for=c1).count()
    assert 8 == MessageAction.objects.actions_for(c1).count()
    assert 2 == MessageAction.objects.actions_for(c1, "action4").count()

    c2_actions = MessageAction.objects.actions_for(c2)
    assert 4 == c2_actions.count()
    assert [c2ma5.pk, c2ma6.pk, c2ma11.pk, c2ma12.pk] == [
        ma.pk for ma in c2_actions
    ]

    c2_actions = MessageAction.objects.actions_for(c2, "action2")
    assert 2 == c2_actions.count()
    assert [c2ma6.pk, c2ma12.pk] == [ma.pk for ma in c2_actions]

    assert 0 == MessageAction.objects.actions_for(c2, "action4").count()


@pytest.mark.django_db
@override_settings(
    NOTIFIABLE_ACTIONS=("action1", "action2", "action3", "action4")
)
def test_message_models_messageaction_per_contact_summary():
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory()
    m2 = MessageFactory()
    c1ma1 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action1"
    )
    c1ma2 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action2"
    )
    c1ma3 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action3"
    )
    c1ma4 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action4"
    )
    c2ma5 = MessageActionFactory(
        message=m1, action_for=c2, action_name="action1"
    )
    c2ma6 = MessageActionFactory(
        message=m1, action_for=c2, action_name="action2"
    )
    c1ma7 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action1"
    )
    c1ma8 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action2"
    )
    c1ma9 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action3"
    )
    c1ma10 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action4"
    )
    c2ma11 = MessageActionFactory(
        message=m2, action_for=c2, action_name="action1"
    )
    c2ma12 = MessageActionFactory(
        message=m2, action_for=c2, action_name="action2"
    )
    assert 4 == MessageAction.objects.actions_for_summary(c1).count()
    assert 1 == MessageAction.objects.actions_for_summary(c1, "action4").count()

    c2_summary = MessageAction.objects.actions_for_summary(c2)
    assert 2 == c2_summary.count()
    assert [2, 2] == [ma["total"] for ma in c2_summary]
    assert ["action1", "action2"] == [ma["action_name"] for ma in c2_summary]

    c2_summary = MessageAction.objects.actions_for_summary(c2, "action2")
    assert 1 == c2_summary.count()
    assert {"action_name": "action2", "total": 2} == c2_summary[0]

    assert 0 == MessageAction.objects.actions_for_summary(c2, "action4").count()


@pytest.mark.django_db
def test_message_models_excluded():
    m1 = MessageFactory()
    m2 = MessageFactory()
    ma1 = MessageActionFactory(message=m1)
    ma2 = MessageActionFactory(message=m1)
    ma3 = MessageActionFactory(message=m1)
    ma4 = MessageActionFactory(message=m2)
    # m1 has three actions
    assert m1.actions().count() == 3
    # m2 has 1 action
    assert m2.actions().count() == 1
    ma3.set_deleted(m1.message_from.user)
    # m1 has 2 actions left
    assert m1.actions().count() == 2
    # m2 still has 1 action
    assert m2.actions().count() == 1
