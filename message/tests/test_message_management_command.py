# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from message.search import MessageIndex
from search.search import SearchIndex
from .factories import MessageFactory


def _create_message():
    message = MessageFactory(message="Hello world", message_to=None)


def _refresh_index():
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 1 == search_index.update()


@pytest.mark.django_db
def test_management_commands_init_app_message():
    call_command("init_app_message")


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_management_commands_rebuild_message_index():
    _create_message()
    _refresh_index()
    call_command("rebuild_message_index")


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_management_commands_refresh_message_index():
    _create_message()
    _refresh_index()
    call_command("refresh_message_index")


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_management_commands_search_message_index():
    _create_message()
    _refresh_index()
    call_command("search_message_index", "world")
