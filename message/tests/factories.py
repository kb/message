# -*- encoding: utf-8 -*-
import factory

from message.models import Message, MessageAction
from contact.tests.factories import ContactFactory


class MessageFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Message

    message_from = factory.SubFactory(ContactFactory)
    message_to = factory.SubFactory(ContactFactory)

    @factory.sequence
    def message(n):
        return (
            """
            Message {:02d}
            """
        ).format(n)


class MessageActionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MessageAction

    message = factory.SubFactory(MessageFactory)
    action_by = factory.SubFactory(ContactFactory)
    action_for = factory.SubFactory(ContactFactory)

    @factory.sequence
    def action_name(n):
        return "action{:02d}".format(n)
