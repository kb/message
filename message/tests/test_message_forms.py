# -*- encoding: utf-8 -*-
import pytest
from django.urls import reverse
from message.forms import MessageForm, NewMessageForm
from message.models import Message
from contact.tests.factories import ContactFactory


@pytest.mark.django_db
def test_message_forms_form():
    conversee1 = ContactFactory()
    conversee2 = ContactFactory()
    data = {
        "message_from": conversee1.pk,
        "message_to": conversee2.pk,
        "message": "Did this test work?",
        "picture": "/message/tests/animals_amphibian_frog.gif",
    }
    form = MessageForm(data=data)
    assert form.is_valid()


@pytest.mark.django_db
def test_message_forms_new_form():
    conversee1 = ContactFactory()
    conversee2 = ContactFactory()
    data = {
        "message_from": conversee1.pk,
        "to_username": conversee2.user.username,
        "message": "Did this test work?",
    }
    form = NewMessageForm(data=data)
    assert form.is_valid()
