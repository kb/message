# -*- encoding: utf-8 -*-
import pytest

from login.tests.fixture import perm_check
from login.tests.scenario import get_user_staff, get_user_web
from django.urls import reverse
from contact.models import Contact
from contact.tests.factories import ContactFactory
from message.models import Message
from .factories import MessageFactory


@pytest.mark.django_db
def test_message_view_perm_conversation_list_secured(perm_check):
    url = reverse("conversation.list")
    perm_check.auth(url)


@pytest.mark.django_db
def test_message_view_perm_new_secured(perm_check):
    url = reverse("conversations.new")
    perm_check.auth(url)


@pytest.mark.django_db
def test_message_view_perm_like_secured(perm_check):
    conversee1 = ContactFactory(user=get_user_staff())
    conversee2 = ContactFactory(user=get_user_web())
    message = MessageFactory(message_from=conversee1, message_to=conversee2)
    url = reverse("conversation.message.like", args=[message.pk])
    redirect_url = reverse("conversation.list", args=[conversee2.pk])
    perm_check.auth(url, expect=302, expect_location=redirect_url)


@pytest.mark.django_db
def test_message_view_perm_unlike_secured(perm_check):
    conversee1 = ContactFactory(user=get_user_staff())
    conversee2 = ContactFactory(user=get_user_web())
    message = MessageFactory(message_from=conversee1, message_to=conversee2)
    url = reverse("conversation.message.unlike", args=[message.pk])
    redirect_url = reverse("conversation.list", args=[conversee2.pk])
    perm_check.auth(url, expect=302, expect_location=redirect_url)


@pytest.mark.django_db
def test_message_view_perm_reported_report_secured(perm_check):
    conversee1 = ContactFactory(user=get_user_staff())
    conversee2 = ContactFactory(user=get_user_web())
    message = MessageFactory(message_from=conversee1, message_to=conversee2)
    url = reverse("reported.report", args=[message.pk])
    redirect_url = reverse("conversation.list", args=[conversee2.pk])
    perm_check.auth(url, expect=302, expect_location=redirect_url)


@pytest.mark.django_db
def test_message_view_perm_reported_list_secured(perm_check):
    conversee1 = ContactFactory(user=get_user_staff())
    conversee2 = ContactFactory(user=get_user_web())
    message = MessageFactory(message_from=conversee1, message_to=conversee2)
    url = reverse("reported.list")
    perm_check.staff(url)


@pytest.mark.django_db
def test_message_view_perm_reported_detail_secured(perm_check):
    conversee1 = ContactFactory(user=get_user_staff())
    conversee2 = ContactFactory(user=get_user_web())
    message = MessageFactory(message_from=conversee1, message_to=conversee2)
    url = reverse("reported.detail", args=[message.pk])
    perm_check.staff(url)


@pytest.mark.django_db
def test_message_view_perm_reported_overrule_secured(perm_check):
    conversee1 = ContactFactory(user=get_user_staff())
    conversee2 = ContactFactory(user=get_user_web())
    message = MessageFactory(message_from=conversee1, message_to=conversee2)
    url = reverse("reported.overruled", args=[message.pk])
    redirect_url = reverse("reported.detail", args=[message.pk])
    perm_check.staff(url, expect=302, expect_location=redirect_url)


@pytest.mark.django_db
def test_message_view_perm_reported_sustain_secured(perm_check):
    conversee1 = ContactFactory(user=get_user_staff())
    conversee2 = ContactFactory(user=get_user_web())
    message = MessageFactory(message_from=conversee1, message_to=conversee2)
    url = reverse("reported.sustained", args=[message.pk])
    redirect_url = reverse("reported.detail", args=[message.pk])
    perm_check.staff(url, expect=302, expect_location=redirect_url)


@pytest.mark.django_db
def test_message_view_perm_reported_postpone_secured(perm_check):
    conversee1 = ContactFactory(user=get_user_staff())
    conversee2 = ContactFactory(user=get_user_web())
    message = MessageFactory(message_from=conversee1, message_to=conversee2)
    url = reverse("reported.postpone", args=[message.pk])
    redirect_url = reverse("reported.list")
    perm_check.staff(url, expect=302, expect_location=redirect_url)
