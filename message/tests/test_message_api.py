# -*- encoding: utf-8 -*-
import json
import pytest

from django.test import override_settings
from django.urls import reverse
from rest_framework import status

from api.tests.fixture import api_client_auth
from contact.tests.factories import ContactFactory
from message.models import Message, MessageAction
from message.search import MessageIndex
from message.tests.factories import MessageFactory, MessageActionFactory
from search.search import SearchIndex

# Shorthand message types
PRIV = Message.MESSAGE_TYPE_PRIVATE
PUBL = Message.MESSAGE_TYPE_PUBLIC
MINE = "Mine"
TRIB = "Tribute"
WALL = "Wall"

# Shorthand actions
REPLIED = MessageAction.REPLIED
LIKED = MessageAction.LIKED
REPORT = MessageAction.REPORT


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_curation(api_client_auth):
    """Confirm all request by curid."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m11 = MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    m12 = MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    m13 = MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    m14 = MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    m15 = MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    m21 = MessageFactory(message_from=cXXc, message_to=c2, message_type=PRIV)
    m22 = MessageFactory(message_from=c2, message_to=cXXc, message_type=PRIV)
    assert 7 == index.update()
    # c1 VIEWS a message list
    response = api_client_auth(c1.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("messages-list"), cXXc.pk, PRIV
        ),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # c1 VIEWS messages with cXXc
    assert 5 == len(data)
    # c1 VIEWS a message list curating c2
    response = api_client_auth(c1.user).get(
        "{}?curid={}&profile_id={}&message_type={}".format(
            reverse("messages-list"), c2.pk, cXXc.pk, PRIV
        ),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # c1 VIEWS messages curating c2 between c2 and cXXc
    assert 2 == len(data)


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_search_between_two_contacts_with_type(api_client_auth):
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    # Private by default.
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    # c4 messages should not be included in the results.
    m7 = MessageFactory(message_from=c1, message_to=c4, message="c1 c4")
    m8 = MessageFactory(message_from=c2, message_to=c4, message="c2 c4")
    m9 = MessageFactory(message_from=c3, message_to=c4, message="c3 c4")
    # TRIB messages should not be included.
    MessageFactory(message_from=c1, message_to=c2, message_type=TRIB)
    MessageFactory(message_from=c2, message_to=c1, message_type=TRIB)
    MessageFactory(message_from=c1, message_to=c2, message_type=TRIB)
    MessageFactory(message_from=c1, message_to=c3, message_type=TRIB)
    MessageFactory(message_from=c3, message_to=c1, message_type=TRIB)
    MessageFactory(message_from=c2, message_to=c3, message_type=TRIB)
    # WALL messages should not be included in the results.
    MessageFactory(message_from=c1, message_type=WALL)
    MessageFactory(message_from=c2, message_type=WALL)
    MessageFactory(message_from=c3, message_type=WALL)
    # Only include PRIV messages
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 18 == search_index.update()
    # Search for messages between c1 and c2.
    response = api_client_auth(c1.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("messages-list"), c2.pk, PRIV
        ),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert 3 == len(data)
    # Search for messages between c2 and c1.
    response = api_client_auth(c2.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("messages-list"), c1.pk, PRIV
        ),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert 3 == len(data)
    # Search for messages between c1 and c3.
    response = api_client_auth(c1.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("messages-list"), c3.pk, PRIV
        ),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert 2 == len(data)
    # Search for messages between c2 and c3.
    response = api_client_auth(c2.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("messages-list"), c3.pk, PRIV
        ),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert 1 == len(data)


@pytest.mark.django_db
def test_message_api_contacts_get(api_client_auth):
    """Confirm contacts list."""
    cXXc = ContactFactory()
    c1 = ContactFactory()
    response = api_client_auth(cXXc.user).get(
        reverse("contacts-list"), content_type="application/vnd.api+json"
    )
    assert status.HTTP_200_OK == response.status_code, response.data


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_anyones_get(api_client_auth):
    """Gets all the messages of a type."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1c1 = MessageFactory(message_from=cXXc, message_type=WALL)
    m2c1 = MessageFactory(message_from=c1, message_type=WALL)
    m1c2 = MessageFactory(message_from=cXXc, message_type=WALL)
    MessageFactory(message_from=c1, message_to=c2, message_type=PRIV)
    MessageFactory(message_from=c2, message_to=c1, message_type=PRIV)
    assert 5 == index.update()
    response = api_client_auth(cXXc.user).get(
        "{}?message_type={}".format(reverse("walls-list"), WALL),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # There are only two conversations
    assert 3 == len(data)
    # Check all mine message
    assert [m1c2.pk, m2c1.pk, m1c1.pk] == [d["id"] for d in data]
    # Full compliment of data
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("created", "notfound") == "notfound"
    assert not data[0].get("id", "notfound") == "notfound"
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("message_from", "notfound") == "notfound"
    assert not data[0].get("message_style", "notfound") == "notfound"
    assert not data[0].get("message_to", "notfound") == "notfound"
    assert not data[0].get("message_type", "notfound") == "notfound"
    assert not data[0].get("picture", "notfound") == "notfound"


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_anyones_delete(api_client_auth):
    """Confirm messages are deleted and softly."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    m1 = MessageFactory()
    assert 1 == index.update()
    # verify presense
    assert Message.objects.all().count() == 1
    response = api_client_auth(c1.user).delete(
        reverse("walls-detail", args=[m1.pk]), content_type="application/json"
    )
    assert status.HTTP_204_NO_CONTENT == response.status_code, response.data
    # verify deletion
    assert 0 == Message.objects.current().count()
    # verify soft deletion
    assert 1 == Message.objects.all().count()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_anyones_post(api_client_auth):
    """Confirm adding a new message."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    c2 = ContactFactory()
    assert 0 == index.update()
    assert 0 == index.count()
    data = {
        "message": WALL,
        "message_from": c1.pk,
        "message_style": "plain",
        "message_to": c2.pk,
        "message_type": WALL,
        "picture": "",
    }
    response = api_client_auth(c2.user).post(
        reverse("walls-list"), json.dumps(data), content_type="application/json"
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    message = Message.objects.all()[0]
    assert WALL == message.message
    assert c1 == message.message_from
    assert "plain" == message.message_style
    assert c2 == message.message_to
    assert WALL == message.message_type


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_anyones_put(api_client_auth):
    """Confirm new message shows as already viewed by poster."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=cXXc, message_type=WALL)
    # Add a few others. We only want 1 message to put.
    MessageFactory(message_from=cXXc, message_to=c1, message_type=WALL)
    MessageFactory(message_from=c1, message_to=cXXc, message_type=WALL)
    MessageFactory(message_from=cXXc, message_to=c1, message_type=WALL)
    assert 4 == index.update()
    data = {
        "created": "2018-09-09T02:36:04.761604+01:00",
        "id": m1.pk,
        "message": WALL,
        "message_from": m1.message_from.pk,
        "message_style": "plain",
        "message_to": m1.message_to.pk,
        "message_type": WALL,
        "picture": "",
    }
    # cXXc views last message sent by cXXc
    response = api_client_auth(cXXc.user).put(
        reverse("walls-detail", args=[m1.pk]),
        json.dumps(data),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    m1.refresh_from_db()
    # The last message has been put
    assert WALL == m1.message
    assert "plain" == m1.message_style
    assert WALL == m1.message_type


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_belonging_to_get(api_client_auth):
    """Gets all the messages directed at or belonging to the specified user."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1c1 = MessageFactory(message_from=cXXc, message_to=c1, message_type=MINE)
    m2c1 = MessageFactory(message_from=c1, message_to=cXXc, message_type=MINE)
    m1c2 = MessageFactory(message_from=cXXc, message_to=c2, message_type=MINE)
    MessageFactory(message_from=c1, message_to=c2, message_type=MINE)
    MessageFactory(message_from=c2, message_to=c1, message_type=MINE)
    assert 5 == index.update()
    response = api_client_auth(cXXc.user).get(
        "{}?message_type={}".format(reverse("mine-list"), MINE),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # There are only two conversations
    assert 3 == len(data)
    # Check all mine message
    assert cXXc.pk == data[2]["message_from"]
    assert c1.pk == data[2]["message_to"]
    assert m1c1.message in data[2]["message"]
    # Second message in dict is last message between cXXc and c1
    assert c1.pk == data[1]["message_from"]
    assert cXXc.pk == data[1]["message_to"]
    assert m2c1.message in data[1]["message"]
    # Second message in dict is last message between cXXc and c1
    assert cXXc.pk == data[0]["message_from"]
    assert c2.pk == data[0]["message_to"]
    assert m1c2.message in data[0]["message"]
    # Full compliment of data
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("created", "notfound") == "notfound"
    assert not data[0].get("id", "notfound") == "notfound"
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("message_from", "notfound") == "notfound"
    assert not data[0].get("message_style", "notfound") == "notfound"
    assert not data[0].get("message_to", "notfound") == "notfound"
    assert not data[0].get("message_type", "notfound") == "notfound"
    assert not data[0].get("picture", "notfound") == "notfound"


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_belonging_to_delete(api_client_auth):
    """Confirm messages are deleted and softly."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    m1 = MessageFactory()
    assert 1 == index.update()
    # verify presense
    assert Message.objects.all().count() == 1
    response = api_client_auth(c1.user).delete(
        reverse("mine-detail", args=[m1.pk]), content_type="application/json"
    )
    assert status.HTTP_204_NO_CONTENT == response.status_code, response.data
    # verify deletion
    assert 0 == Message.objects.current().count()
    # verify soft deletion
    assert 1 == Message.objects.all().count()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_belonging_to_post(api_client_auth):
    """Confirm adding a new message."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    c2 = ContactFactory()
    assert 0 == index.update()
    assert 0 == index.count()
    data = {
        "message": MINE,
        "message_from": c1.pk,
        "message_style": "plain",
        "message_to": c2.pk,
        "message_type": MINE,
        "picture": "",
    }
    response = api_client_auth(c2.user).post(
        reverse("mine-list"), json.dumps(data), content_type="application/json"
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    message = Message.objects.all()[0]
    assert MINE == message.message
    assert c1 == message.message_from
    assert "plain" == message.message_style
    assert c2 == message.message_to
    assert MINE == message.message_type


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_belonging_to_put(api_client_auth):
    """Confirm new message shows as already viewed by poster."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=cXXc, message_type=MINE)
    # Add a few others. We only want 1 message to put.
    MessageFactory(message_from=cXXc, message_to=c1, message_type=MINE)
    MessageFactory(message_from=c1, message_to=cXXc, message_type=MINE)
    MessageFactory(message_from=cXXc, message_to=c1, message_type=MINE)
    assert 4 == index.update()
    data = {
        "created": "2018-09-09T02:36:04.761604+01:00",
        "id": m1.pk,
        "message": MINE,
        "message_from": m1.message_from.pk,
        "message_style": "plain",
        "message_to": m1.message_to.pk,
        "message_type": MINE,
        "picture": "",
    }
    # cXXc views last message sent by cXXc
    response = api_client_auth(cXXc.user).put(
        reverse("mine-detail", args=[m1.pk]),
        json.dumps(data),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    m1.refresh_from_db()
    # The last message has been put
    assert MINE == m1.message
    assert "plain" == m1.message_style
    assert MINE == m1.message_type


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_between_two_get(api_client_auth):
    """Confirm message list."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1c1 = MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    m2c1 = MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    m1c2 = MessageFactory(message_from=cXXc, message_to=c2, message_type=PRIV)
    assert 3 == index.update()
    response = api_client_auth(cXXc.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("messages-list"), c1.pk, PRIV
        ),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # There are only two messages in the conversation between cXXc and c1
    assert 2 == len(data)
    # First message in dict is first message between cXXc and c1
    assert cXXc.pk == data[0]["message_from"]
    assert c1.pk == data[0]["message_to"]
    assert m1c1.message in data[0]["message"]
    # Second message in dict is last message between cXXc and c1
    assert c1.pk == data[1]["message_from"]
    assert cXXc.pk == data[1]["message_to"]
    assert m2c1.message in data[1]["message"]
    # Full compliment of data
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("created", "notfound") == "notfound"
    assert not data[0].get("id", "notfound") == "notfound"
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("message_from", "notfound") == "notfound"
    assert not data[0].get("message_style", "notfound") == "notfound"
    assert not data[0].get("message_to", "notfound") == "notfound"
    assert not data[0].get("message_type", "notfound") == "notfound"
    assert not data[0].get("picture", "notfound") == "notfound"


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_directed_at_get_list(api_client_auth):
    """Gets all the messages directed at the specified user."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1c1 = MessageFactory(message_from=c1, message_to=cXXc, message_type=TRIB)
    m2c1 = MessageFactory(message_from=c2, message_to=cXXc, message_type=TRIB)
    m1c2 = MessageFactory(message_from=cXXc, message_to=c2, message_type=TRIB)
    MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    MessageFactory(message_from=c2, message_to=cXXc, message_type=PRIV)
    assert 5 == index.update()
    response = api_client_auth(cXXc.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("tributes-list"), cXXc.pk, TRIB
        ),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # There are only two conversations
    assert 2 == len(data)
    # Check all mine message
    # First message in dict is last message between cXXc and c2
    assert c2.pk == data[0]["message_from"]
    assert cXXc.pk == data[0]["message_to"]
    assert m2c1.pk == data[0]["id"]
    assert m2c1.message in data[0]["message"]
    # Second message in dict is last message between cXXc and c1
    assert c1.pk == data[1]["message_from"]
    assert cXXc.pk == data[1]["message_to"]
    assert m1c1.pk == data[1]["id"]
    assert m1c1.message in data[1]["message"]
    # Full compliment of data
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("created", "notfound") == "notfound"
    assert not data[0].get("id", "notfound") == "notfound"
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("message_from", "notfound") == "notfound"
    assert not data[0].get("message_style", "notfound") == "notfound"
    assert not data[0].get("message_to", "notfound") == "notfound"
    assert not data[0].get("message_type", "notfound") == "notfound"
    assert not data[0].get("picture", "notfound") == "notfound"
    # Look at another user.
    response = api_client_auth(cXXc.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("tributes-list"), c2.pk, TRIB
        ),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # There is only 1 conversations
    assert 1 == len(data)
    # Check all mine message
    assert cXXc.pk == data[0]["message_from"]
    assert c2.pk == data[0]["message_to"]
    assert m1c2.pk == data[0]["id"]
    assert m1c2.message in data[0]["message"]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_directed_at_delete(api_client_auth):
    """Confirm messages are deleted and softly."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    m1 = MessageFactory()
    assert 1 == index.update()
    # verify presense
    assert Message.objects.all().count() == 1
    response = api_client_auth(c1.user).delete(
        reverse("tributes-detail", args=[m1.pk]),
        content_type="application/json",
    )
    assert status.HTTP_204_NO_CONTENT == response.status_code, response.data
    # verify deletion
    assert 0 == Message.objects.current().count()
    # verify soft deletion
    assert 1 == Message.objects.all().count()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_directed_at_post(api_client_auth):
    """Confirm adding a new message."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    c2 = ContactFactory()
    assert 0 == index.update()
    assert 0 == index.count()
    data = {
        "message": TRIB,
        "message_from": c1.pk,
        "message_style": "plain",
        "message_to": c2.pk,
        "message_type": TRIB,
        "picture": "",
    }
    response = api_client_auth(c2.user).post(
        reverse("tributes-list"),
        json.dumps(data),
        content_type="application/json",
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    message = Message.objects.all()[0]
    assert TRIB == message.message
    assert c1 == message.message_from
    assert "plain" == message.message_style
    assert c2 == message.message_to
    assert TRIB == message.message_type


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_directed_at_put(api_client_auth):
    """Confirm new message shows as already viewed by poster."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=cXXc, message_type=TRIB)
    # Add a few others. We only want 1 message to put.
    MessageFactory(message_from=cXXc, message_to=c1, message_type=TRIB)
    MessageFactory(message_from=c1, message_to=cXXc, message_type=TRIB)
    MessageFactory(message_from=cXXc, message_to=c1, message_type=TRIB)
    assert 4 == index.update()
    data = {
        "created": "2018-09-09T02:36:04.761604+01:00",
        "id": m1.pk,
        "message": TRIB,
        "message_from": m1.message_from.pk,
        "message_style": "plain",
        "message_to": m1.message_to.pk,
        "message_type": TRIB,
        "picture": "",
    }
    # cXXc views last message sent by cXXc
    response = api_client_auth(cXXc.user).put(
        reverse("tributes-detail", args=[m1.pk]),
        json.dumps(data),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    m1.refresh_from_db()
    # The last message has been put
    assert TRIB == m1.message
    assert "plain" == m1.message_style
    assert TRIB == m1.message_type


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_directed_at_auth_user(api_client_auth):
    """Gets all the messages directed at the specified user."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1c1 = MessageFactory(message_from=c1, message_to=cXXc, message_type=TRIB)
    m2c1 = MessageFactory(message_from=c2, message_to=cXXc, message_type=TRIB)
    m1c2 = MessageFactory(message_from=cXXc, message_to=c2, message_type=TRIB)
    MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    MessageFactory(message_from=c2, message_to=cXXc, message_type=PRIV)
    assert 5 == index.update()
    # Run the same test twice.

    def consistent_test(response):
        assert status.HTTP_200_OK == response.status_code, response.data
        data = response.json()
        # There are only two conversations
        assert 2 == len(data)
        # Check all mine message
        # First message in dict is last message between cXXc and c2
        assert c2.pk == data[0]["message_from"]
        assert cXXc.pk == data[0]["message_to"]
        assert m2c1.pk == data[0]["id"]
        assert m2c1.message in data[0]["message"]
        # Second message in dict is last message between cXXc and c1
        assert c1.pk == data[1]["message_from"]
        assert cXXc.pk == data[1]["message_to"]
        assert m1c1.pk == data[1]["id"]
        assert m1c1.message in data[1]["message"]
        return True

    # With the "message_to" query_param
    response = api_client_auth(cXXc.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("tributes-list"), cXXc.pk, TRIB
        ),
        content_type="application/vnd.api+json",
    )
    assert consistent_test(response)
    # Repeat without the "message_to" query_param
    response = api_client_auth(cXXc.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("tributes-list"), cXXc.pk, TRIB
        ),
        content_type="application/vnd.api+json",
    )
    assert consistent_test(response)


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_lastofeach(api_client_auth):
    """Confirm conversations are last-message-per-person for request-contact."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    MessageFactory(message_from=cXXc, message_to=c2, message_type=PRIV)
    MessageFactory(message_from=c2, message_to=cXXc, message_type=PRIV)
    MessageFactory(message_from=cXXc, message_to=c2, message_type=PRIV)
    lmc2 = MessageFactory(message_from=c2, message_to=cXXc, message_type=PRIV)
    lmc1 = MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    assert 7 == index.update()
    response = api_client_auth(cXXc.user).get(
        "{}?message_type={}".format(reverse("conversations-list"), PRIV),
        content_type="application/vnd.api+json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # There are only two conversations
    assert 2 == len(data)
    # First message in dict is last message ever
    assert cXXc.pk == data[0]["message_from"]
    assert c1.pk == data[0]["message_to"]
    assert lmc1.pk == data[0]["id"]
    assert lmc1.message in data[0]["message"]
    # Second message in dict is last message between cXXc and c1
    assert c2.pk == data[1]["message_from"]
    assert cXXc.pk == data[1]["message_to"]
    assert lmc2.pk == data[1]["id"]
    assert lmc2.message in data[1]["message"]
    # Full compliment of data
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("created", "notfound") == "notfound"
    assert not data[0].get("id", "notfound") == "notfound"
    assert not data[0].get("message", "notfound") == "notfound"
    assert not data[0].get("message_from", "notfound") == "notfound"
    assert not data[0].get("message_style", "notfound") == "notfound"
    assert not data[0].get("message_to", "notfound") == "notfound"
    assert not data[0].get("message_type", "notfound") == "notfound"
    assert not data[0].get("picture", "notfound") == "notfound"


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_messages_delete(api_client_auth):
    """Confirm messages are deleted and softly."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    m1 = MessageFactory()
    assert 1 == index.update()
    # verify presense
    assert 1 == Message.objects.all().count()
    response = api_client_auth(c1.user).delete(
        reverse("messages-detail", args=[m1.pk]),
        content_type="application/json",
    )
    assert status.HTTP_204_NO_CONTENT == response.status_code, response.data
    # verify deletion
    assert 0 == Message.objects.current().count()
    # verify soft deletion
    assert 1 == Message.objects.all().count()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_messages_put(api_client_auth):
    """Confirm new message shows as already viewed by poster."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    # Add a few others. We only want 1 message to put.
    MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    m2 = MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    assert 4 == index.update()
    data = {
        "created": "2018-09-09T02:36:04.761604+01:00",
        "id": m1.pk,
        "message": "Hello",
        "message_from": m1.message_from.pk,
        "message_style": "plain",
        "message_to": m1.message_to.pk,
        "message_type": PRIV,
        "picture": "",
    }
    # cXXc views last message sent by cXXc
    response = api_client_auth(cXXc.user).put(
        reverse("messages-detail", args=[m1.pk]),
        json.dumps(data),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    m1.refresh_from_db()
    # The last message has been patched
    assert "Hello" == m1.message
    assert "plain" == m1.message_style
    assert PRIV == m1.message_type


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_messages_post(api_client_auth):
    """Confirm adding a new message."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    c2 = ContactFactory()
    assert 0 == index.update()
    assert 0 == index.count()
    data = {
        "message": "Hello",
        "message_from": c1.pk,
        "message_style": "plain",
        "message_to": c2.pk,
        "message_type": PRIV,
        "picture": "",
    }
    response = api_client_auth(c2.user).post(
        reverse("messages-list"),
        json.dumps(data),
        content_type="application/json",
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    message = Message.objects.all()[0]
    assert "Hello" == message.message
    assert c1 == message.message_from
    assert "plain" == message.message_style
    assert c2 == message.message_to
    assert PRIV == message.message_type


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_messages_show_action_summary(api_client_auth):
    """Confirm message list serialized message action summary per message."""
    index = SearchIndex(MessageIndex())
    index.drop_create()

    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message_type=PRIV)
    m2 = MessageFactory(message_from=c2, message_to=c1, message_type=PRIV)
    ma1 = MessageActionFactory(message=m1, action_for=c2, action_name=LIKED)
    ma2 = MessageActionFactory(message=m1, action_for=c3, action_name=LIKED)
    ma3 = MessageActionFactory(message=m1, action_for=c4, action_name=REPORT)
    ma4 = MessageActionFactory(message=m2, action_for=c1, action_name=REPORT)
    ma5 = MessageActionFactory(message=m2, action_for=c3, action_name=REPORT)
    ma6 = MessageActionFactory(message=m2, action_for=c4, action_name=LIKED)
    assert 2 == index.update()

    def consistent_test(response):
        data = response.json()
        # Assert same result ...
        assert data[0]["id"] == m1.pk
        assert data[0]["action_summary"] == [
            {"action_name": LIKED, "total": 2},
            {"action_name": REPORT, "total": 1},
        ]
        assert data[1]["id"] == m2.pk
        assert data[1]["action_summary"] == [
            {"action_name": LIKED, "total": 1},
            {"action_name": REPORT, "total": 2},
        ]
        # ... but different "was_liked" due to contact change to c2  (hint: m1).
        # assert data[0]["was_liked"]
        # assert not data[1]["was_liked"]
        return True

    # Let c1 make the request
    response = api_client_auth(c1.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("messages-list"), c2.pk, PRIV
        ),
        content_type="application/json",
    )
    assert consistent_test(response)
    # Let c2 make the request
    response = api_client_auth(c2.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("messages-list"), c1.pk, PRIV
        ),
        content_type="application/json",
    )
    assert consistent_test(response)


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_messages_show_actions_by(api_client_auth):
    """Confirm message list serialized request-contact actions per message."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    m1 = MessageFactory(message_type=WALL)
    m2 = MessageFactory(message_type=WALL)
    ma1 = MessageActionFactory(message=m1, action_by=c1, action_name=LIKED)
    ma2 = MessageActionFactory(message=m1, action_by=c2, action_name=LIKED)
    ma3 = MessageActionFactory(message=m1, action_by=c1, action_name=REPORT)
    ma4 = MessageActionFactory(message=m2, action_by=c1, action_name=LIKED)
    # We shouldn't see any of these actions by different contacts
    MessageActionFactory(message=m1, action_by=c3, action_name=LIKED)
    MessageActionFactory(message=m2, action_by=c3, action_name=LIKED)
    MessageActionFactory(message=m2, action_by=c4, action_name=LIKED)
    MessageActionFactory(message=m2, action_by=c3, action_name=REPORT)
    assert 2 == index.update()
    # Let c1 make the request
    response = api_client_auth(c1.user).get(
        "{}?message_type={}".format(reverse("messages-list"), WALL),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # assert messages array is correct.
    assert [m1.pk, m2.pk] == [d["id"] for d in data]
    # assert c1 2 actions are built into the wall posts
    assert 2 == len(data[0]["actions_by"])
    assert {
        "action_by": c1.pk,
        "action_for": ma1.action_for.pk,
        "action_name": LIKED,
        "dismissed": False,
        "id": ma1.pk,
        "message": m1.pk,
    } == data[0]["actions_by"][0]
    assert {
        "action_by": c1.pk,
        "action_for": ma3.action_for.pk,
        "action_name": REPORT,
        "dismissed": False,
        "id": ma3.id,
        "message": m1.pk,
    } == data[0]["actions_by"][1]

    assert 1 == len(data[1]["actions_by"])
    assert [
        {
            "action_by": c1.pk,
            "action_for": ma4.action_for.pk,
            "action_name": LIKED,
            "dismissed": False,
            "id": ma4.id,
            "message": m2.pk,
        }
    ] == data[1]["actions_by"]
    # Let c2 make the request
    response = api_client_auth(c2.user).get(
        "{}?message_type={}".format(reverse("messages-list"), WALL),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # assert messages array is correct.
    assert [m1.pk, m2.pk] == [d["id"] for d in data]
    # assert c2 actions are showing against the message list
    assert 1 == len(data[0]["actions_by"])
    assert [
        {
            "action_by": c2.pk,
            "action_for": ma2.action_for.pk,
            "action_name": LIKED,
            "dismissed": False,
            "id": ma2.pk,
            "message": m1.pk,
        }
    ] == data[0]["actions_by"]
    assert 0 == len(data[1]["actions_by"])


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_messageaction_get(api_client_auth):
    """Confirm retrival of actions."""
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    c5 = ContactFactory()
    ma1 = MessageActionFactory(action_for=c2, action_name=LIKED)
    ma2 = MessageActionFactory(action_for=c3, action_name=LIKED)
    ma3 = MessageActionFactory(action_for=c4, action_name=LIKED)
    ma4 = MessageActionFactory(action_for=c1, action_name=LIKED)
    ma5 = MessageActionFactory(action_for=c3, action_name=LIKED)
    ma6 = MessageActionFactory(action_for=c4, action_name=LIKED)
    response = api_client_auth(c1.user).get(
        "{}?profile_id={}".format(reverse("actions-list"), c1.pk),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert [
        {
            "action_by": ma4.action_by.pk,
            "action_for": c1.pk,
            "action_name": LIKED,
            "dismissed": False,
            "id": ma4.pk,
            "message": ma4.message.pk,
        }
    ] == data
    response = api_client_auth(c2.user).get(
        "{}?profile_id={}".format(reverse("actions-list"), c2.pk),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert [
        {
            "action_by": ma1.action_by.pk,
            "action_for": c2.pk,
            "action_name": LIKED,
            "dismissed": False,
            "id": ma1.pk,
            "message": ma1.message.pk,
        }
    ] == data
    response = api_client_auth(c3.user).get(
        "{}?profile_id={}".format(reverse("actions-list"), c3.pk),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert [
        {
            "action_by": ma2.action_by.pk,
            "action_for": c3.pk,
            "action_name": LIKED,
            "dismissed": False,
            "id": ma2.pk,
            "message": ma2.message.pk,
        },
        {
            "action_by": ma5.action_by.pk,
            "action_for": c3.pk,
            "action_name": LIKED,
            "dismissed": False,
            "id": ma5.pk,
            "message": ma5.message.pk,
        },
    ] == data
    response = api_client_auth(c4.user).get(
        "{}?profile_id={}".format(reverse("actions-list"), c4.pk),
        content_type="application/json",
    )
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    assert [
        {
            "action_by": ma3.action_by.pk,
            "action_for": c4.pk,
            "action_name": LIKED,
            "dismissed": False,
            "id": ma3.pk,
            "message": ma3.message.pk,
        },
        {
            "action_by": ma6.action_by.pk,
            "action_for": c4.pk,
            "action_name": LIKED,
            "dismissed": False,
            "id": ma6.pk,
            "message": ma6.message.pk,
        },
    ] == data


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_messageaction_post(api_client_auth):
    """Confirm can add new message actions."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory(message_from=c2)
    assert 1 == index.update()
    data = {
        "action_by": c1.pk,
        "action_for": c2.pk,
        "action_name": LIKED,
        "dismissed": False,
        "message": m1.pk,
    }
    response = api_client_auth(c1.user).post(
        "{}?profile_id={}".format(reverse("actions-list"), c1.pk),
        json.dumps(data),
        content_type="application/json",
    )
    assert status.HTTP_201_CREATED == response.status_code, response.data
    ma1 = MessageAction.objects.all()[0]
    data = response.json()
    assert data == {
        "action_by": c1.pk,
        "action_for": c2.pk,
        "action_name": LIKED,
        "dismissed": False,
        "id": ma1.pk,
        "message": m1.pk,
    }
    qry = m1.action_summary()
    assert LIKED == qry[0]["action_name"]
    assert 1 == qry[0]["total"]
    assert 1 == m1.actions_by(c1).count()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_messageaction_delete(api_client_auth):
    """Confirm can delete message actions."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    c1 = ContactFactory()
    m1 = MessageFactory(message_from=c1)
    ma1 = MessageActionFactory(message=m1, action_for=c1)
    assert 1 == index.update()
    # verify presense
    qry = m1.action_summary()
    assert qry.count() == 1
    response = api_client_auth(c1.user).delete(
        reverse("actions-detail", args=[ma1.pk]),
        content_type="application/json",
    )
    assert status.HTTP_204_NO_CONTENT == response.status_code, response.data
    # verify deletion
    qry = m1.action_summary()
    assert 0 == qry.count()
    # verify no soft deletion
    assert 0 == MessageAction.objects.all().count()


@pytest.mark.django_db
@pytest.mark.elasticsearch
@override_settings(
    NOTIFIABLE_ACTIONS=("action1", "action2", "action3", "action4")
)
def test_message_api_messageaction_contact_notifications(api_client_auth):
    """Confirm status api node returns contact's actions."""
    c1 = ContactFactory()
    m1 = MessageFactory()
    m2 = MessageFactory()
    c1ma1 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action1"
    )
    c1ma2 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action2"
    )
    c1ma3 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action3"
    )
    c1ma7 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action1"
    )
    c1ma8 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action2"
    )
    c1ma9 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action4"
    )
    response = api_client_auth(c1.user).get(
        reverse("notifications-list"), content_type="application/json"
    )
    data = response.json()
    assert data == [
        {"action_name": "action1", "total": 2},
        {"action_name": "action2", "total": 2},
        {"action_name": "action3", "total": 1},
        {"action_name": "action4", "total": 1},
    ]


@pytest.mark.django_db
@pytest.mark.elasticsearch
@override_settings(
    NOTIFIABLE_ACTIONS=("action1", "action2", "action3", "action4")
)
def test_message_api_messageaction_contact_notifications_curated(
    api_client_auth
):
    """Confirm status api node returns contact's actions."""
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory()
    m2 = MessageFactory()
    c1ma1 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action1"
    )
    c1ma2 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action2"
    )
    c1ma3 = MessageActionFactory(
        message=m1, action_for=c1, action_name="action3"
    )
    c1ma7 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action1"
    )
    c1ma8 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action2"
    )
    c1ma9 = MessageActionFactory(
        message=m2, action_for=c1, action_name="action4"
    )
    response = api_client_auth(c2.user).get(
        "{}?curid={}".format(reverse("notifications-list"), c1.pk),
        content_type="application/json",
    )
    data = response.json()
    assert data == [
        {"action_name": "action1", "total": 2},
        {"action_name": "action2", "total": 2},
        {"action_name": "action3", "total": 1},
        {"action_name": "action4", "total": 1},
    ]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_mixin_action_dismiss(api_client_auth):
    """Confirm all conversation messages show "dismissed" by request-contact."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory(company_name="cXXc")
    c1 = ContactFactory(company_name="c1")
    c2 = ContactFactory(company_name="c2")
    m11 = MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    m12 = MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    m21 = MessageFactory(message_from=cXXc, message_to=c2, message_type=PRIV)
    m22 = MessageFactory(message_from=c2, message_to=cXXc, message_type=PRIV)
    assert 4 == index.update()
    ma12 = MessageActionFactory(
        message=m12, action_by=c1, action_for=cXXc, action_name=REPLIED
    )
    ma22 = MessageActionFactory(
        message=m22, action_by=c2, action_for=cXXc, action_name=REPLIED
    )

    # Establish what we know: cXXc has 2 REPLIED messages undismissed.
    assert 2 == (MessageAction.objects.filter(dismissed=False).count())

    # Get cXXc current status
    response = api_client_auth(cXXc.user).get(
        reverse("actions-list"), content_type="application/json"
    )
    # API response integral
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # 2 message action notifications for cXXc
    assert 2 == len(data)
    assert [
        {
            "action_by": c1.pk,
            "action_for": cXXc.pk,
            "action_name": REPLIED,
            "dismissed": False,
            "id": ma12.pk,
            "message": m12.pk,
        },
        {
            "action_by": c2.pk,
            "action_for": cXXc.pk,
            "action_name": REPLIED,
            "dismissed": False,
            "id": ma22.pk,
            "message": m22.pk,
        },
    ] == data
    # cXXc VIEWS the message list between c1 therefore DISMISSES the notification
    response = api_client_auth(cXXc.user).get(
        "{}?profile_id={}&message_type={}".format(
            reverse("messages-list"), c1.pk, PRIV
        ),
        content_type="application/json",
    )
    # API response integral
    assert status.HTTP_200_OK == response.status_code, response.data
    # Get cXXc status
    response = api_client_auth(cXXc.user).get(
        "{}?profile_id={}".format(reverse("actions-list"), cXXc.pk),
        content_type="application/json",
    )
    # API response integral
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # Leaves only the c2 message action notification
    assert 1 == len(data)
    assert [
        {
            "action_by": c2.pk,
            "action_for": cXXc.pk,
            "action_name": REPLIED,
            "dismissed": False,
            "id": ma22.pk,
            "message": m22.pk,
        }
    ] == data
    # 1 message dismissed
    assert 1 == (MessageAction.objects.filter(dismissed=True).count())


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_api_mixin_action_dismiss_curated(api_client_auth):
    """Confirm all conversation messages show "dismissed" by curating-contact."""
    index = SearchIndex(MessageIndex())
    index.drop_create()
    cXXc = ContactFactory(company_name="cXXc")
    c1 = ContactFactory(company_name="c1")
    c2 = ContactFactory(company_name="c2")
    m11 = MessageFactory(message_from=cXXc, message_to=c1, message_type=PRIV)
    m12 = MessageFactory(message_from=c1, message_to=cXXc, message_type=PRIV)
    m21 = MessageFactory(message_from=cXXc, message_to=c2, message_type=PRIV)
    m22 = MessageFactory(message_from=c2, message_to=cXXc, message_type=PRIV)
    assert 4 == index.update()
    ma12 = MessageActionFactory(
        message=m12, action_by=c1, action_for=cXXc, action_name=REPLIED
    )
    ma22 = MessageActionFactory(
        message=m22, action_by=c2, action_for=cXXc, action_name=REPLIED
    )

    # Establish what we know: cXXc has 2 REPLIED messages undismissed.
    assert 2 == (MessageAction.objects.filter(dismissed=False).count())

    # Get cXXc current status
    response = api_client_auth(c2.user).get(
        "{}?curid={}".format(reverse("actions-list"), cXXc.pk),
        content_type="application/json",
    )
    # API response integral
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # 2 message action notifications for cXXc
    assert 2 == len(data)
    assert [
        {
            "action_by": c1.pk,
            "action_for": cXXc.pk,
            "action_name": REPLIED,
            "dismissed": False,
            "id": ma12.pk,
            "message": m12.pk,
        },
        {
            "action_by": c2.pk,
            "action_for": cXXc.pk,
            "action_name": REPLIED,
            "dismissed": False,
            "id": ma22.pk,
            "message": m22.pk,
        },
    ] == data
    # CURATED by c2, cXXc VIEWS the message list between c1 therefore DISMISSES the notification
    response = api_client_auth(c2.user).get(
        "{}?curid={}&profile_id={}&message_type={}".format(
            reverse("messages-list"), cXXc.pk, c1.pk, PRIV
        ),
        content_type="application/json",
    )
    # API response integral
    assert status.HTTP_200_OK == response.status_code, response.data
    # Get cXXc status
    response = api_client_auth(c2.user).get(
        "{}?curid={}".format(reverse("actions-list"), cXXc.pk),
        content_type="application/json",
    )
    # API response integral
    assert status.HTTP_200_OK == response.status_code, response.data
    data = response.json()
    # Leaves only the c2 message action notification
    assert 1 == len(data)
    assert [
        {
            "action_by": c2.pk,
            "action_for": cXXc.pk,
            "action_name": REPLIED,
            "dismissed": False,
            "id": ma22.pk,
            "message": m22.pk,
        }
    ] == data
    # 1 message dismissed
    assert 1 == (MessageAction.objects.filter(dismissed=True).count())
