# -*- encoding: utf-8 -*-
import json
import pytest

from dateutil.relativedelta import relativedelta
from django.utils import timezone
from contact.models import Contact, ContactAddress
from contact.tests.factories import ContactAddressFactory, ContactFactory
from login.tests.factories import UserFactory
from message.models import Message, MessageAction
from message.search import (
    search_helper_messages_between_contacts,
    search_helper_messages_belonging_to,
    search_helper_messages_directed_to,
    search_helper_messages_type,
    MessageIndex,
    MessageIndexMixin,
    Row,
)
from message.tests.factories import MessageFactory, MessageActionFactory
from search.models import SearchFormat
from search.search import SearchIndex


PRIV = Message.MESSAGE_TYPE_PRIVATE
PUBL = Message.MESSAGE_TYPE_PUBLIC
TRIB = "trib"
WALL = "wall"
NO_CRITERIA = ""


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_result_length():
    """Returns all results."""
    c1 = ContactFactory()
    c2 = ContactFactory()
    for x in range(0, 50):
        m1 = MessageFactory(message_from=c1, message_to=c1, message_type=TRIB)
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 50 == search_index.update()
    must_extra = [search_helper_messages_type(TRIB)]
    result, total = search_index.search(
        "",
        must_extra=must_extra,
        data_format=SearchFormat.COLUMN,
        page_size=1000,
    )
    assert 50 == total
    assert 50 == len(result)


class MessageSoftDeleteIndex(MessageIndexMixin):
    @property
    def is_soft_delete(self):
        return True


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_data_as_attr():
    c1 = ContactFactory(
        company_name=NO_CRITERIA,
        user=UserFactory(
            first_name="Adam", last_name="Applewood", email="adam@applewood.com"
        ),
    )
    ContactAddressFactory(contact=c1, town="London", country="UK")
    c2 = ContactFactory(
        company_name="Zero Ltd", user=UserFactory(email="enquiries@zero.fr")
    )
    ContactAddressFactory(contact=c2, town="Paris", country="FR")
    m1 = MessageFactory(
        message_from=c1, message_to=c2, message="sunny dry summer"
    )
    m2 = MessageFactory(
        message_from=c2, message_to=c1, message="cold wet winter"
    )
    m3 = MessageFactory(
        message_from=c1, message_to=c2, message="sunny wet spring"
    )
    index = SearchIndex(MessageIndex())
    index.drop_create()
    assert 3 == index.update()
    result, total = index.search("dry", data_format=SearchFormat.ATTR)
    assert 1 == total
    assert [m1.pk] == [x.pk for x in result]
    assert [
        Row(
            message="sunny dry summer",
            message_from=str(c1.pk),
            message_to=str(c2.pk),
            message_type=PRIV,
        )
    ] == [x.data for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_drop_create():
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_null_criteria_or_extras():
    """Basic Test with and without criteria."""
    CRITERIA = "hot"
    MATCH1 = "hot summer"
    m1 = MessageFactory(message=NO_CRITERIA)
    m2 = MessageFactory(message=MATCH1)
    m3 = MessageFactory(message=MATCH1)
    m4 = MessageFactory(message=NO_CRITERIA)
    m5 = MessageFactory(message=MATCH1)
    m6 = MessageFactory(message=MATCH1)
    index = SearchIndex(MessageIndex())
    index.drop_create()
    assert 6 == index.update()
    # Criteria
    message_type_must = search_helper_messages_type(PRIV)
    result, total = index.search(
        CRITERIA,
        must_extra=[message_type_must],
        data_format=SearchFormat.COLUMN,
    )
    assert 4 == total
    assert [m2.pk, m3.pk, m5.pk, m6.pk] == [x.pk for x in result]
    # No criteria
    result, total = index.search(
        NO_CRITERIA,
        must_extra=[message_type_must],
        data_format=SearchFormat.COLUMN,
    )
    assert 6 == total
    assert [m1.pk, m2.pk, m3.pk, m4.pk, m5.pk, m6.pk] == [x.pk for x in result]
    # No criteria or must_extra or should_extra.
    result, total = index.search(NO_CRITERIA, data_format=SearchFormat.COLUMN)
    assert 0 == total
    # Criteria without must_extra or should_extra.
    result, total = index.search(CRITERIA, data_format=SearchFormat.COLUMN)
    assert 4 == total


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_basics():
    CRITERIA = "wet"
    MATCH1 = "cold {} winter".format(CRITERIA)
    MATCH2 = "sunny {} spring".format(CRITERIA)
    NOMATCH = "sunny dry summer"
    c1 = ContactFactory(
        company_name=NO_CRITERIA,
        user=UserFactory(
            first_name="Adam", last_name="Applewood", email="adam@applewood.com"
        ),
    )
    ContactAddressFactory(contact=c1, town="London", country="UK")
    c2 = ContactFactory(
        company_name="Zero Ltd", user=UserFactory(email="enquiries@zero.fr")
    )
    ContactAddressFactory(contact=c2, town="Paris", country="FR")
    m1 = MessageFactory(message_from=c1, message_to=c2, message=NOMATCH)
    m2 = MessageFactory(message_from=c2, message_to=c1, message=MATCH1)
    m3 = MessageFactory(message_from=c1, message_to=c2, message=MATCH2)
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 3 == search_index.update()
    result, total = search_index.search(
        CRITERIA, data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [m2.pk, m3.pk] == [x.pk for x in result]
    assert ["message", "message"] == [x.document_type for x in result]
    check = []
    for row in result:
        data = row.data
        col_list = []
        for column in data:
            line_list = []
            for line in column:
                line_list.append(line.text)
            col_list.append(line_list)
        check.append(col_list)
    assert [
        [[MATCH1], [str(c2.pk)], [str(c1.pk)], [PRIV]],
        [[MATCH2], [str(c1.pk)], [str(c2.pk)], [PRIV]],
    ] == check


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_deleted_hard_delete():
    """The ``MessageIndex`` will actually delete documents."""
    CRITERIA = "hot"
    MATCH1 = "hot summer"
    NO_MATCH = "long winter"
    user = UserFactory()
    m1 = MessageFactory(message=MATCH1)
    m2 = MessageFactory(message=MATCH1)
    m3 = MessageFactory(message=NO_MATCH)
    m4 = MessageFactory(message=MATCH1)
    m2.set_deleted(user)
    index = SearchIndex(MessageIndex())
    index.drop_create()
    assert 3 == index.update()
    # exclude deleted
    result, total = index.search(CRITERIA, data_format=SearchFormat.COLUMN)
    assert 2 == total
    assert [m1.pk, m4.pk] == [x.pk for x in result]
    # include deleted - should be ignored!!
    result, total = index.search(
        CRITERIA, include_deleted=True, data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [m1.pk, m4.pk] == [x.pk for x in result]


@pytest.mark.skip(reason="See comment FAILS BECAUSE in test")
@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_deleted_soft_delete():
    """The ``MessageSoftDeleteIndex`` will just mark documents as deleted."""
    CRITERIA = "hot"
    MATCH1 = "hot summer"
    NO_MATCH = "long winter"
    user = UserFactory()
    m1 = MessageFactory(message=MATCH1)
    m2 = MessageFactory(message=MATCH1)
    m3 = MessageFactory(message=NO_MATCH)
    m4 = MessageFactory(message=MATCH1)
    m2.set_deleted(user)
    index = SearchIndex(MessageSoftDeleteIndex())
    index.drop_create()
    assert 4 == index.update()
    # exclude deleted
    result, total = index.search(CRITERIA, data_format=SearchFormat.COLUMN)
    assert 2 == total
    """FAILS BECAUSE::

        # app/search/search.py
        bool_data.update(
            {
                "filter": {"term": {"is_deleted": False}},
                "minimum_should_match": 1, #<-- Message CRITERIA goes into "must"
            }
        )
    """
    assert [m1.pk, m4.pk] == [x.pk for x in result]
    # include deleted messages (but not contacts)
    # Fails because the include_deleted filter goes into should.
    result, total = index.search(
        CRITERIA, include_deleted=True, data_format=SearchFormat.COLUMN
    )
    assert 3 == total
    assert [m1.pk, m2.pk, m4.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_update():
    MessageFactory()
    MessageFactory()
    MessageFactory()
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 3 == search_index.update()


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_of_type():
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message_type=PRIV)
    m2 = MessageFactory(message_from=c2, message_to=c1, message_type=TRIB)
    m3 = MessageFactory(message_from=c1, message_to=c2, message_type=TRIB)
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 3 == search_index.update()
    # Search for TRIB messages.
    must_extra = [search_helper_messages_type(TRIB)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [m2.pk, m3.pk] == [x.pk for x in result]
    # Search for PRIV messages.
    message_type_must = search_helper_messages_type(PRIV)
    must_extra = [message_type_must]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 1 == total
    assert [m1.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_helper_messages_belonging_to_helper():
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 6 == search_index.update()
    # Search for c1's messages.
    must_extra = [search_helper_messages_belonging_to(c1.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 5 == total
    assert [m1.pk, m2.pk, m3.pk, m4.pk, m5.pk] == [x.pk for x in result]
    # Search for c2's messages.
    must_extra = [search_helper_messages_belonging_to(c2.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 4 == total
    assert [m1.pk, m2.pk, m3.pk, m6.pk] == [x.pk for x in result]
    # Search for c3's messages.
    must_extra = [search_helper_messages_belonging_to(c3.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 3 == total
    assert [m4.pk, m5.pk, m6.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_helper_messages_between_contacts_helper():
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    # c4 messages should not be included in the results.
    m7 = MessageFactory(message_from=c1, message_to=c4, message="c1 c4")
    m8 = MessageFactory(message_from=c2, message_to=c4, message="c2 c4")
    m9 = MessageFactory(message_from=c3, message_to=c4, message="c3 c4")
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 9 == search_index.update()
    # Search for messages between c1 and c2.
    must_extra = [search_helper_messages_between_contacts(c1.pk, c2.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 3 == total
    assert [m1.pk, m2.pk, m3.pk] == [x.pk for x in result]
    # Search for messages between c1 and c3.
    must_extra = [search_helper_messages_between_contacts(c1.pk, c3.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [m4.pk, m5.pk] == [x.pk for x in result]
    # Search for messages between c2 and c3.
    must_extra = [search_helper_messages_between_contacts(c2.pk, c3.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 1 == total
    assert [m6.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_helper_messages_between_contacts_helper_with_type():
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    # Private by default.
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    # c4 messages should not be included in the results.
    m7 = MessageFactory(message_from=c1, message_to=c4, message="c1 c4")
    m8 = MessageFactory(message_from=c2, message_to=c4, message="c2 c4")
    m9 = MessageFactory(message_from=c3, message_to=c4, message="c3 c4")
    # TRIB messages should not be included.
    MessageFactory(message_from=c1, message_to=c2, message_type=TRIB)
    MessageFactory(message_from=c2, message_to=c1, message_type=TRIB)
    MessageFactory(message_from=c1, message_to=c2, message_type=TRIB)
    MessageFactory(message_from=c1, message_to=c3, message_type=TRIB)
    MessageFactory(message_from=c3, message_to=c1, message_type=TRIB)
    MessageFactory(message_from=c2, message_to=c3, message_type=TRIB)
    # WALL messages should not be included in the results.
    MessageFactory(message_from=c1, message_type=WALL)
    MessageFactory(message_from=c2, message_type=WALL)
    MessageFactory(message_from=c3, message_type=WALL)
    # Only include PRIV messages
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 18 == search_index.update()
    # Search for messages between c1 and c2.
    # Tests are the same as `test_message_search_messages_between_two_users`
    # except we've added the PRIV type.
    must_extra = [
        search_helper_messages_between_contacts(c1.pk, c2.pk),
        search_helper_messages_type(PRIV),
    ]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 3 == total
    assert [m1.pk, m2.pk, m3.pk] == [x.pk for x in result]
    # Search for messages between c1 and c3.
    must_extra = [
        search_helper_messages_between_contacts(c1.pk, c3.pk),
        search_helper_messages_type(PRIV),
    ]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [m4.pk, m5.pk] == [x.pk for x in result]
    # Search for messages between c2 and c3.
    must_extra = [
        search_helper_messages_between_contacts(c2.pk, c3.pk),
        search_helper_messages_type(PRIV),
    ]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 1 == total
    assert [m6.pk] == [x.pk for x in result]


@pytest.mark.django_db
@pytest.mark.elasticsearch
def test_message_search_helper_messages_directed_to_helper():
    c1 = ContactFactory()
    c2 = ContactFactory()
    c3 = ContactFactory()
    c4 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m2 = MessageFactory(message_from=c2, message_to=c1, message="c2 c1")
    m3 = MessageFactory(message_from=c1, message_to=c2, message="c1 c2")
    m4 = MessageFactory(message_from=c1, message_to=c3, message="c1 c3")
    m5 = MessageFactory(message_from=c3, message_to=c1, message="c3 c1")
    m6 = MessageFactory(message_from=c2, message_to=c3, message="c2 c3")
    # c4 messages should not be included in the results.
    m7 = MessageFactory(message_from=c1, message_to=c4, message="c1 c4")
    m8 = MessageFactory(message_from=c2, message_to=c4, message="c2 c4")
    m9 = MessageFactory(message_from=c3, message_to=c4, message="c3 c4")
    search_index = SearchIndex(MessageIndex())
    search_index.drop_create()
    assert 9 == search_index.update()
    # Search for messages directed at c1.
    must_extra = [search_helper_messages_directed_to(c1.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [m2.pk, m5.pk] == [x.pk for x in result]
    # Search for messages directed at c2.
    must_extra = [search_helper_messages_directed_to(c2.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [m1.pk, m3.pk] == [x.pk for x in result]
    # Search for messages directed at c3.
    must_extra = [search_helper_messages_directed_to(c3.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 2 == total
    assert [m4.pk, m6.pk] == [x.pk for x in result]
    # Search for messages directed at c4.
    must_extra = [search_helper_messages_directed_to(c4.pk)]
    result, total = search_index.search(
        NO_CRITERIA, must_extra=must_extra, data_format=SearchFormat.COLUMN
    )
    assert 3 == total
    assert [m7.pk, m8.pk, m9.pk] == [x.pk for x in result]
