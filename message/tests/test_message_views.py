# -*- encoding: utf-8 -*-
import pytest
from django.urls import reverse
from http import HTTPStatus
from contact.tests.factories import ContactFactory
from login.tests.factories import TEST_PASSWORD
from login.tests.scenario import (
    default_scenario_login,
    get_user_staff,
    get_user_web,
)
from message.models import Message, MessageAction
from message.tests.factories import MessageFactory, MessageActionFactory


@pytest.mark.django_db
def test_message_views_conversations_list(client):
    """Test the conversation list renders when there are no messages."""
    conversee1 = ContactFactory()
    conversee2 = ContactFactory()
    # Log in
    assert (
        client.login(username=conversee1.user.username, password=TEST_PASSWORD)
        is True
    )
    # Normal list
    url = reverse("conversation.list")
    response = client.get(url)
    assert 200 == response.status_code
    assert "conversations" in response.context
    assert "active_user" in response.context
    assert "conversation" not in response.context
    assert "conversee" not in response.context


@pytest.mark.django_db
def test_message_views_conversations_with_conversation_list(client):
    """Test the conversation list renders."""
    conversee1 = ContactFactory()
    conversee2 = ContactFactory()
    for x in range(1, 10):
        MessageFactory(message_from=conversee2, message_to=conversee1)
        MessageFactory(message_from=conversee1, message_to=conversee2)
    # Log in
    assert (
        client.login(username=conversee1.user.username, password=TEST_PASSWORD)
        is True
    )
    # Normal list
    url = reverse("conversation.list")
    response = client.get(url)
    assert 200 == response.status_code
    assert "conversations" in response.context
    assert "active_user" in response.context
    assert "conversation" not in response.context
    assert "conversee" not in response.context
    # With a user selected
    url = reverse("conversation.list", args=[conversee2.pk])
    response = client.get(url)
    assert 200 == response.status_code
    assert "conversations" in response.context
    assert "active_user" in response.context
    assert "conversation" in response.context
    assert "conversee" in response.context
    # Adding a new messageo
    url = reverse("conversation.list", args=[conversee2.pk])
    validated_data = {
        "message_from": conversee1.pk,
        "message_to": conversee2.pk,
        "message": "Did this test work?",
    }
    response = client.post(url, validated_data)
    assert 302 == response.status_code
    assert url == response["Location"]


@pytest.mark.django_db
def test_message_views_new_conversation(client):
    """Create a new conversation with a previously unmessaged user."""
    conversee1 = ContactFactory()
    conversee2 = ContactFactory()
    for x in range(1, 10):
        MessageFactory(message_from=conversee2, message_to=conversee1)
        MessageFactory(message_from=conversee1, message_to=conversee2)
    assert (
        client.login(username=conversee1.user.username, password=TEST_PASSWORD)
        is True
    )
    # Adding a new conversation by selecting a username to send a message to.
    url = reverse("conversations.new")
    response = client.get(url)
    assert 200 == response.status_code
    validated_data = {
        "message_from": conversee1.pk,
        "to_username": conversee2.user.username,
        "message": "Did this test work?",
        "picture": "/message/tests/animals_amphibian_frog.gif",
    }
    response = client.post(url, validated_data)
    assert 302 == response.status_code


@pytest.mark.django_db
def test_message_views_message_detail(client):
    """Visit the detail of a reported message."""
    default_scenario_login()
    c1 = ContactFactory(user=get_user_staff())
    ma1 = MessageActionFactory(action_name=MessageAction.REPORT)
    assert (
        client.login(username=c1.user.username, password=TEST_PASSWORD) is True
    )
    # Liked message 1
    url = reverse("message.detail", args=[ma1.message.pk])
    response = client.get(url)
    assert 200 == response.status_code
    assert "message" in response.context


@pytest.mark.django_db
def test_message_views_reported_list(client):
    """Visit the list of messages which have been reported."""
    default_scenario_login()
    c1 = ContactFactory(user=get_user_staff())
    ma1 = MessageActionFactory(action_name=MessageAction.REPORT)
    assert (
        client.login(username=c1.user.username, password=TEST_PASSWORD) is True
    )
    # Liked message 1
    url = reverse("reported.list")
    response = client.get(url)
    assert 200 == response.status_code
    assert "message_list" in response.context
    assert 1 == len(response.context["message_list"])


@pytest.mark.django_db
def test_message_views_reported_detail(client):
    """Visit the detail of a reported message."""
    default_scenario_login()
    c1 = ContactFactory(user=get_user_staff())
    ma1 = MessageActionFactory(action_name=MessageAction.REPORT)
    assert (
        client.login(username=c1.user.username, password=TEST_PASSWORD) is True
    )
    # Liked message 1
    url = reverse("reported.detail", args=[ma1.message.pk])
    response = client.get(url)
    assert 200 == response.status_code
    assert "actions" in response.context
    assert "message" in response.context


@pytest.mark.django_db
def test_message_views_reported_report(client):
    """Report posts."""
    default_scenario_login()
    c1 = ContactFactory(user=get_user_web())
    m1 = MessageFactory(message_from=c1)
    assert (
        client.login(username=c1.user.username, password=TEST_PASSWORD) is True
    )
    assert not MessageAction.objects.current().count()
    url = reverse("reported.report", args=[m1.pk])
    response = client.get(url)
    # Redirects after unliking post
    assert 302 == response.status_code
    mas = MessageAction.objects.current()
    assert mas.count() == 1
    assert [ma.action_name for ma in mas] == [MessageAction.REPORT]
    assert set([ma.action_by for ma in mas]) == {c1}


@pytest.mark.django_db
def test_message_views_reported_overruled(client):
    """Report posts."""
    default_scenario_login()
    c1 = ContactFactory(user=get_user_staff())
    m1 = MessageFactory(message_from=c1)
    assert (
        client.login(username=c1.user.username, password=TEST_PASSWORD) is True
    )
    assert not MessageAction.objects.current().count()
    url = reverse("reported.overruled", args=[m1.pk])
    response = client.get(url)
    # Redirects after unliking post
    assert 302 == response.status_code
    mas = MessageAction.objects.current()
    assert mas.count() == 1
    assert [ma.action_name for ma in mas] == [MessageAction.OVERRULE]
    assert set([ma.action_by for ma in mas]) == {c1}


@pytest.mark.django_db
def test_message_views_reported_sustained(client):
    """Report posts."""
    default_scenario_login()
    c1 = ContactFactory(user=get_user_staff())
    m1 = MessageFactory(message_from=c1)
    assert (
        client.login(username=c1.user.username, password=TEST_PASSWORD) is True
    )
    assert not MessageAction.objects.current().count()
    url = reverse("reported.sustained", args=[m1.pk])
    response = client.get(url)
    # Redirects after unliking post
    assert 302 == response.status_code
    mas = MessageAction.objects.current()
    assert mas.count() == 1
    assert [ma.action_name for ma in mas] == [MessageAction.SUSTAIN]
    assert set([ma.action_by for ma in mas]) == {c1}


@pytest.mark.django_db
def test_message_views_reported_postpone_when_sustained(client):
    """Report posts."""
    default_scenario_login()
    c1 = ContactFactory(user=get_user_staff())
    m1 = MessageFactory(message_from=c1)
    MessageActionFactory(
        action_by=c1,
        action_for=m1.message_from,
        message=m1,
        action_name=MessageAction.REPORT,
    )
    MessageActionFactory(
        action_by=c1,
        action_for=m1.message_from,
        message=m1,
        action_name=MessageAction.SUSTAIN,
    )
    assert (
        client.login(username=c1.user.username, password=TEST_PASSWORD) is True
    )
    url = reverse("reported.postpone", args=[m1.pk])
    response = client.get(url)
    assert 302 == response.status_code
    mas = MessageAction.objects.current()
    # Even if SUSTAIN, PostPone will remove any decisions
    assert mas.count() == 1
    assert [ma.action_name for ma in mas] == [MessageAction.REPORT]
    assert set([ma.action_by for ma in mas]) == {c1}


@pytest.mark.django_db
def test_message_views_reported_postpone_when_overruled(client):
    """Report posts."""
    default_scenario_login()
    c1 = ContactFactory(user=get_user_staff())
    m1 = MessageFactory(message_from=c1)
    MessageActionFactory(
        action_by=c1,
        action_for=m1.message_from,
        message=m1,
        action_name=MessageAction.REPORT,
    )
    MessageActionFactory(
        action_by=c1,
        action_for=m1.message_from,
        message=m1,
        action_name=MessageAction.OVERRULE,
    )
    assert (
        client.login(username=c1.user.username, password=TEST_PASSWORD) is True
    )
    url = reverse("reported.postpone", args=[m1.pk])
    response = client.get(url)
    assert 302 == response.status_code
    mas = MessageAction.objects.current()
    # Even if OVERRULED, PostPone will remove any decisions
    assert mas.count() == 1
    assert [ma.action_name for ma in mas] == [MessageAction.REPORT]
    assert set([ma.action_by for ma in mas]) == {c1}


@pytest.mark.django_db
def test_message_views_like(client):
    """Liked and unlike posts."""
    publ = Message.MESSAGE_TYPE_PUBLIC
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_type=publ)
    m2 = MessageFactory(message_from=c2, message_type=publ)
    assert (
        client.login(username=c2.user.username, password=TEST_PASSWORD) is True
    )
    assert not MessageAction.objects.current().count()
    # Liked message 1
    url = reverse("conversation.message.like", args=[m1.pk])
    response = client.get(url)
    # Redirects after liking post
    assert 302 == response.status_code
    mas = MessageAction.objects.current()
    assert mas.count() == 1
    assert [ma.action_name for ma in mas] == [MessageAction.LIKED]
    assert set([ma.action_by for ma in mas]) == {c2}


@pytest.mark.django_db
def test_message_views_unlike(client):
    """Liked and unlike posts."""
    publ = Message.MESSAGE_TYPE_PUBLIC
    like = MessageAction.LIKED
    c1 = ContactFactory()
    c2 = ContactFactory()
    m1 = MessageFactory(message_from=c1, message_type=publ)
    assert (
        client.login(username=c2.user.username, password=TEST_PASSWORD) is True
    )
    MessageAction.objects.add_message_action(m1, c2, c1, like)
    assert MessageAction.objects.current().count() == 1
    # Unlike message 1
    url = reverse("conversation.message.unlike", args=[m1.pk])
    response = client.get(url)
    # Redirects after unliking post
    assert 302 == response.status_code
    assert not MessageAction.objects.current().count()
