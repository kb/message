# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from contact.search import ContactIndex
from search.models import SearchFormat
from search.search import SearchIndex


class Command(BaseCommand):

    help = "Query (search) contact index"

    def add_arguments(self, parser):
        parser.add_argument("criteria", nargs="+", type=str)

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        index = SearchIndex(ContactIndex())
        for criteria in options["criteria"]:
            result, total = index.search(
                criteria, data_format=SearchFormat.ATTR, page_size=100
            )
            self.stdout.write(
                "\nFound {} records searching for '{}'\n\n".format(
                    total, criteria
                )
            )
            self.stdout.write(
                "{:20.18} {:40.38} {:10.8} {:20.18} {}".format(
                    "name", "address", "postcode", "email", "score"
                )
            )
            self.stdout.write(
                (
                    "-------------------- "
                    "---------------------------------------- "
                    "---------- "
                    "-------------------- "
                    "-----------"
                )
            )
            for row in result:
                self.stdout.write(
                    "{:20.18} {:40.38} {:10.8} {:20.18} {}".format(
                        row.data.name,
                        row.data.address,
                        row.data.postcode,
                        row.data.email,
                        row.score,
                    )
                )
        self.stdout.write("{} - Complete".format(self.help))
