# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from message.tasks import rebuild_message_index


class Command(BaseCommand):

    help = "Rebuild message index"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        rebuild_message_index()
        self.stdout.write("{} - Complete".format(self.help))
