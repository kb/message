# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from contact.tasks import refresh_contact_index


class Command(BaseCommand):

    help = "Refresh contact index"

    def handle(self, *args, **options):
        self.stdout.write("{}".format(self.help))
        refresh_contact_index()
        self.stdout.write("{} - Complete".format(self.help))
