# -*- encoding: utf-8 -*-
from django.db.models import Q
from .models import Message
from .search import (
    search_helper_build_query_from_results,
    search_helper_messages_belonging_to,
    search_helper_messages_between_contacts,
    search_helper_messages_directed_to,
    search_helper_messages_type,
    MessageIndex,
)


def search_anyones_of_type(criteria, message_type):
    """Utility: Search for all the messages of a certain type."""

    if criteria:
        from search.models import SearchFormat
        from search.search import SearchIndex

        index = SearchIndex(MessageIndex())
        # Must: Limit to messages between these two people.
        must_extra = []
        if message_type:
            must_extra += [search_helper_messages_type(message_type)]
        result, count = index.search(
            criteria,
            must_extra=must_extra,
            data_format=SearchFormat.COLUMN,
            page_size=1000,
        )
        qs = search_helper_build_query_from_results(
            Message.objects.current(), result
        )
    else:
        qs = Message.objects.current().filter(message_type=message_type)
    return qs


def search_belonging_to(criteria, profile_id, message_type):
    """Utility: Search the last message of conversations with other contacts."""

    if criteria:
        from search.models import SearchFormat
        from search.search import SearchIndex

        index = SearchIndex(MessageIndex())
        page_size = 10000
        if criteria:
            page_size = 100
        # Must: Limit to messages to or from the active_user
        must_extra = []
        if message_type:
            must_extra += [search_helper_messages_type(message_type)]
        must_extra += [search_helper_messages_belonging_to(profile_id)]
        result, count = index.search(
            criteria,
            must_extra=must_extra,
            data_format=SearchFormat.COLUMN,
            page_size=page_size,
        )
        qs = search_helper_build_query_from_results(
            Message.objects.current(), result
        )
    else:
        qs = (
            Message.objects.current()
            .filter(message_type=message_type)
            .filter(Q(message_from_id=profile_id) | Q(message_to_id=profile_id))
        )
    return qs


def search_between_two_contacts(
    criteria, active_user_pk, profile_id, message_type
):
    """Utility: Search for the conversation thread between two contacts."""
    from search.models import SearchFormat
    from search.search import SearchIndex

    index = SearchIndex(MessageIndex())
    page_size = 10000
    if criteria:
        page_size = 100
    # Must: Limit to messages between these two people.
    must_extra = []
    if message_type:
        must_extra += [search_helper_messages_type(message_type)]
    must_extra += [
        search_helper_messages_between_contacts(active_user_pk, profile_id)
    ]
    result, count = index.search(
        criteria,
        must_extra=must_extra,
        data_format=SearchFormat.COLUMN,
        page_size=page_size,
    )
    qs = search_helper_build_query_from_results(
        Message.objects.current(), result
    )
    return qs


def search_directed_to(criteria, profile_id, message_type):
    """Utility: Search the last message of conversations with other contacts."""
    if criteria:
        from search.models import SearchFormat
        from search.search import SearchIndex

        index = SearchIndex(MessageIndex())
        page_size = 10000
        if criteria:
            page_size = 100
        # Must: Limit to messages to or from the active_user
        must_extra = []
        if message_type:
            must_extra += [search_helper_messages_type(message_type)]
        must_extra += [search_helper_messages_directed_to(profile_id)]
        result, count = index.search(
            criteria,
            must_extra=must_extra,
            data_format=SearchFormat.COLUMN,
            page_size=page_size,
        )
        qs = search_helper_build_query_from_results(
            Message.objects.current(), result
        )
    else:
        qs = Message.objects.current().filter(
            message_to_id=profile_id, message_type=message_type
        )
    return qs


def search_lastofeach_per_contact(criteria, active_user_pk, message_type):
    """Utility: Search the last message of conversations with other contacts."""
    from search.models import SearchFormat
    from search.search import SearchIndex

    index = SearchIndex(MessageIndex())
    # Must: Limit to messages to or from the active_user
    must_extra = []
    if message_type:
        must_extra += [search_helper_messages_type(message_type)]
    must_extra += [search_helper_messages_belonging_to(active_user_pk)]
    result, count = index.search(
        criteria,
        must_extra=must_extra,
        data_format=SearchFormat.COLUMN,
        page_size=1000,
    )
    # The conversations qs will be the last message per contact.
    contact_last_result = {}
    for res in result:
        # The other contact for each message.
        other_user_pk = (
            res.data[1][0].text
            if res.data[2][0].text == str(active_user_pk)
            else res.data[2][0].text
        )
        # Dictionary of contacts storing the last message id of each.
        contact_last_result[other_user_pk] = res.pk
    # Use the dictionary to filter the results
    last_message_pk_per_contact = []
    for k, v in contact_last_result.items():
        last_message_pk_per_contact.append(contact_last_result[k])
    # Get the last message from each user. Sorted with the latest at the top.
    qs = (
        Message.objects.current()
        .filter(pk__in=last_message_pk_per_contact)
        .order_by("-created")
    )
    return qs
