# -*- encoding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from base.form_utils import FileDropInput
from .models import Message


class MessageForm(forms.ModelForm):
    """For sending messages between two users."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["message_to"].widget = forms.HiddenInput()
        self.fields["message_from"].widget = forms.HiddenInput()
        self.fields["message"].widget.attrs.update(
            {"class": "pure-input-1", "rows": 3}
        )
        self.fields["picture"].widget = FileDropInput(
            default_text="Drop your picture here...",
            click_text="or click here to select your picture",
        )

    class Meta:
        model = Message
        fields = ("message_from", "message_to", "message", "picture")


class NewMessageForm(MessageForm):
    """For sending messages between two users."""

    to_username = forms.CharField(required=True, label="User")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        to_username = cleaned_data.get("to_username").strip()
        if to_username:
            raise_error = False
            try:
                user = get_user_model().objects.get(username=to_username)
            except get_user_model().DoesNotExist:
                raise_error = True
            if raise_error:
                raise forms.ValidationError(
                    "No user called {} found".format(to_username),
                    code="message__message_to__notexists",
                )
        return cleaned_data
