# -*- encoding: utf-8 -*-
import attr
import json

from django.db.models import Case, When
from message.models import Message
from search.search import Fmt


def search_helper_messages_belonging_to(belongs_to_pk):
    return {
        "bool": {
            "should": [
                {"match": {"message_from_pk": belongs_to_pk}},
                {"match": {"message_to_pk": belongs_to_pk}},
            ]
        }
    }


def search_helper_messages_between_contacts(contact1_pk, contact2_pk):
    return {
        "bool": {
            "should": [
                {
                    "bool": {
                        "must": [
                            {"match": {"message_from_pk": contact1_pk}},
                            {"match": {"message_to_pk": contact2_pk}},
                        ]
                    }
                },
                {
                    "bool": {
                        "must": [
                            {"match": {"message_from_pk": contact2_pk}},
                            {"match": {"message_to_pk": contact1_pk}},
                        ]
                    }
                },
            ]
        }
    }


def search_helper_messages_directed_to(directed_to_pk):
    return {"bool": {"must": [{"match": {"message_to_pk": directed_to_pk}}]}}


def search_helper_messages_type(messages_type):
    return {"bool": {"must": [{"match": {"message_type": messages_type}}]}}


def search_helper_build_query_from_results(model_query, result):
    pks = []
    for row in result:
        pks.append(row.pk)
    preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(pks)])
    qs = model_query.filter(pk__in=pks).order_by(preserved)
    return qs


@attr.s
class Row:
    """For use with 'data_as_attr'."""

    message = attr.ib()
    message_from = attr.ib()
    message_to = attr.ib()
    message_type = attr.ib()


class MessageIndexMixin:
    def _message_from_full_name(self, row):
        if row.message_from:
            return row.message_from.full_name
        else:
            return ""

    def _message_to_full_name(self, row):
        if row.message_to:
            return row.message_to.full_name
        else:
            return ""

    def _settings(self):
        """Index settings."""
        return {
            "number_of_shards": 1,
            "analysis": {
                "analyzer": {
                    "autocomplete": {
                        "tokenizer": "autocomplete",
                        "filter": ["lowercase", "whitespace_remove"],
                    },
                    "autocomplete_search": {
                        "tokenizer": "keyword",
                        "filter": ["lowercase", "whitespace_remove"],
                    },
                },
                "char_filter": {
                    "digit_only": {
                        "type": "pattern_replace",
                        "pattern": "[^0-9]",
                        "replacbelong_to_searchement": "",
                    }
                },
                "filter": {
                    "whitespace_remove": {
                        "type": "pattern_replace",
                        "pattern": " ",
                        "replacement": "",
                    }
                },
                "tokenizer": {
                    "autocomplete": {
                        "type": "edge_ngram",
                        "min_gram": 1,
                        "max_gram": 20,
                        "token_chars": ["digit", "letter", "whitespace"],
                    }
                },
            },
        }

    @property
    def configuration(self):
        """Index configuration.  Used by the Elasticsearch 'create' method."""
        return {
            "mappings": {
                self.document_type: {
                    "properties": {
                        "created": {"type": "date"},
                        "is_deleted": {"type": "boolean"},
                        "message": {
                            "type": "text",
                            "analyzer": "english",
                            "search_analyzer": "autocomplete_search",
                            "fields": {"keyword": {"type": "keyword"}},
                        },
                        "message_from_pk": {"type": "integer"},
                        "message_to_pk": {"type": "integer"},
                        "pk": {"type": "integer"},
                    }
                }
            },
            "settings": {"index": self._settings()},
        }

    def data_as_attr(self, source, pk):
        """A row of data returned as part of the search results.

        .. note:: Check the ``source`` method.  Not all fields are added to the
                  Elasticsearch index.
        """
        return Row(
            message="{message}".format(**source),
            message_from="{message_from_pk}".format(**source),
            message_to="{message_to_pk}".format(**source),
            message_type="{message_type}".format(**source),
        )

    def data_as_fmt(self, source, pk):
        """A row of data returned as part of the search results."""
        return [
            [Fmt("{message}".format(**source), url=True)],
            [Fmt("{message_from_pk}".format(**source), url=True)],
            [Fmt("{message_to_pk}".format(**source), url=True)],
            [Fmt("{message_type}".format(**source), url=True)],
        ]

    @property
    def document_type(self):
        """Elasticsearch document type."""
        return "message"

    @property
    def index_name(self):
        """Name of the Elasticsearch index (will have the domain added)."""
        return "message"

    def query(self, criteria, should_extra=None, must_extra=None):
        """The Elasticsearch query.

        If there is no criteria/should_extra/must_extra `query` will not
        be hit."""
        should = []
        must = []
        if criteria:
            must += [{"match": {"message": {"query": criteria}}}]

        if should_extra:
            should += should_extra
        if must_extra:
            must += must_extra
        query = {"should": should, "must": must}
        return {
            "query": {"bool": query},
            "sort": [{"created": "asc"}, {"pk": "asc"}],
        }

    def queryset(self, pk):
        """Build the Elasticsearch index using this queryset."""
        if pk is None:
            qs = Message.objects.all().order_by("pk")
        else:
            qs = Message.objects.filter(pk=pk)
        return qs

    def source(self, row):
        """Build the index using this data from the 'queryset'."""
        message_to_pk = 0
        if row.message_to:
            message_to_pk = row.message_to.pk
        return {
            "created": row.created,
            "message": row.message,
            "message_from_pk": row.message_from.pk,
            "message_to_pk": message_to_pk,
            "message_type": row.message_type,
            "pk": row.pk,
        }

    def table_headings(self):
        """Table headings for the results."""
        return ["Message", "From", "To", "Type"]

    def url_name(self):
        """URL name for the search results."""
        return "message.detail"


class MessageIndex(MessageIndexMixin):
    @property
    def is_soft_delete(self):
        return False
