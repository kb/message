# -*- encoding: utf-8 -*-
import re
from dateutil.parser import parse
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import connections, models
from django.db.models import Count, Max, Q
from django.urls import reverse
from reversion import revisions as reversion
from base.model_utils import TimedCreateModifyDeleteModel
from contact.models import Contact


class MessageError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("{}, {}".format(self.__class__.__name__, self.value))


class MessageManager(models.Manager):
    def create_message(self, **kwargs):
        obj = self.model(**kwargs)
        obj.save()
        return obj

    def current(self):
        return self.model.objects.exclude(deleted=True)

    def messages_to(self, message_to, message_type):
        """Finds all the tributes, etc given to a person."""
        return (
            self.current()
            .filter(message_to=message_to, message_type=message_type)
            .order_by("-created")
        )

    def messages_type(self, message_type):
        """Finds all the messages of a given type."""
        return (
            self.current()
            .filter(message_type=message_type)
            .order_by("-created")
        )

    def conversation(self, converser, conversee):
        """Finds all the messages between two given users."""
        conversation = self.current().filter(
            (Q(message_from=converser) & Q(message_to=conversee))
            | (Q(message_to=converser) & Q(message_from=conversee)),
            message_type=self.model.MESSAGE_TYPE_PRIVATE,
        )
        return conversation

    def message_conversation(self, converser, message_id):
        """Finds all the messages with just a single message_id."""
        message = self.model.objects.get(id=message_id)
        # Only do this if the converser is valid!
        if (
            message.message_from.id == converser.id
            or message.message_to.id == converser.id
        ):
            conversee = message.message_from
            if converser.id == conversee.id:
                conversee = message.message_to
            return self.conversation(converser, conversee)
        return []

    def conversations(self, converser):
        """Finds all other users this person is in a conversation with."""
        rows = []
        if converser:
            with connections["default"].cursor() as cursor:
                cursor.execute(
                    """
                    SELECT
                    	MAX(id) as id
                    FROM message_message
                    WHERE
                    	(
                           message_from_id = %s OR
                	       message_to_id = %s
                        ) AND message_type=%s
                        AND deleted=false
                    GROUP BY
                    	CASE message_to_id
                    	  WHEN %s THEN message_from_id
                    	  ELSE message_to_id
                    	END;
                    """,
                    [
                        converser.pk,
                        converser.pk,
                        Message.MESSAGE_TYPE_PRIVATE,
                        converser.pk,
                    ],
                )
                rows = cursor.fetchall()
        last_messages = [m[0] for m in rows]
        return self.current().filter(id__in=last_messages).order_by("-created")


class Message(TimedCreateModifyDeleteModel):
    """Message model."""

    MESSAGE_TYPE_PRIVATE = "Private"
    MESSAGE_TYPE_PUBLIC = "Public"

    message_types = (
        (MESSAGE_TYPE_PRIVATE, "Private"),
        (MESSAGE_TYPE_PUBLIC, "Public"),
    )

    message_from = models.ForeignKey(
        Contact, related_name="message_from_contact", on_delete=models.CASCADE
    )
    message_to = models.ForeignKey(
        Contact,
        related_name="message_to_contact",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    message = models.TextField(blank=True, null=True)
    message_style = models.CharField(
        max_length=20, default="plain", blank=True, null=True
    )
    message_type = models.CharField(
        max_length=20, default=MESSAGE_TYPE_PRIVATE, blank=False, null=False
    )
    picture = models.ImageField(upload_to="message", blank=True, null=True)
    is_public = models.BooleanField(default=False)

    objects = MessageManager()

    class Meta:
        ordering = ["created", "pk"]
        verbose_name = "Message"
        verbose_name_plural = "Messages"

    def __str__(self):
        return "{} to {}: {}".format(
            self.message_to, self.message_from, self.message
        )

    def actions(self):
        """Returns all the actions for this message."""
        return self.messageaction_set.exclude(deleted=True)

    def actions_of_type(self, action_name):
        """Returns all the actions for this message."""
        return self.actions().filter(action_name=action_name)

    def action_summary(self):
        """Return a summary grouped by actions for this message."""
        return (
            self.actions()
            .values("action_name")
            .annotate(total=Count("action_name"))
            .order_by("action_name")
        )

    def actions_by(self, action_by):
        """Returns all the actions on this message for the given contact."""
        return self.actions().filter(action_by=action_by)

    def was_actioned_by(self, action_by, action_name):
        """Tells you whether a contact performed the given action for a
        message."""
        return (
            self.actions_by(action_by).filter(action_name=action_name).count()
            > 0
        )

    def get_mentions(self, url_path):
        """Gets the IDs of any users mentioned in this message."""
        if self.message:
            mentions = [
                re.compile(r"[^\d]+").sub("", i)
                for i in re.findall(
                    "{}[\"'\/](\d+)[\"'\>]".format(url_path), self.message
                )
            ]
            return sorted(list(set([int(m) for m in mentions if m])))
        else:
            return []

    def get_multi_as_dict(self):
        """Return the contact multi models as a dictionary.

        .. note:: For use in a view context.

        """
        return dict(actions=self.actions().order_by("pk"))

    # def get_absolute_url(self):
    #     return reverse("conversation.list", args=[self.message_to.pk])

    def set_deleted(self, user):
        super().set_deleted(user)

    def undelete(self):
        super().undelete()


reversion.register(Message)


class MessageActionManager(models.Manager):
    def current(self):
        return self.model.objects.exclude(deleted=True)

    def add_message_action(self, message, action_by, action_for, action_name):
        """Add an action just once for a message, contact and name."""
        obj = None
        try:
            obj = self.model.objects.get(
                message=message,
                action_by=action_by,
                action_for=action_for,
                action_name=action_name,
            )
            # Take as a reason to undelete this.
            if obj.is_deleted:
                obj.undelete()
        except self.model.DoesNotExist:
            obj = self.model(
                message=message,
                action_by=action_by,
                action_for=action_for,
                action_name=action_name,
            )
            obj.save()
        return obj

    def del_message_action(self, message, action_by, action_for, action_name):
        """Delete an action given message, action_by, action_for and name."""
        obj = None
        try:
            obj = self.model.objects.get(
                message=message,
                action_by=action_by,
                action_for=action_for,
                action_name=action_name,
            )
        except self.model.DoesNotExist:
            pass  # It's okay. No action to delete!
        if obj and not obj.is_deleted:
            obj.delete()
        return obj

    def actions_for(self, action_for, action_name=None):
        """Return all actions to action_for which have not been dimissed."""
        rtn = None
        if action_name:
            rtn = self.model.objects.filter(
                action_for=action_for, action_name=action_name, dismissed=False
            )
        else:
            rtn = self.model.objects.filter(
                action_for=action_for,
                action_name__in=settings.NOTIFIABLE_ACTIONS,
                dismissed=False,
            )
        return rtn

    def actions_for_summary(self, action_for, action_name=None):
        """Return a summary per action_for grouped by actions."""
        return (
            self.actions_for(action_for, action_name)
            .values("action_name")
            .annotate(total=Count("action_name"))
            .order_by("action_name")
        )


class MessageAction(TimedCreateModifyDeleteModel):
    """MessageAction model."""

    # RECORDS of ACTIONS
    BOOKMARK = "Bookmark"  # BOOKMARK a message
    OVERRULE = "Overrule"  # OVERRULE a REPORT
    POSTPONE = "Postpone"  # POSTPONE a REPORT
    REPORT = "Report"  # REPORT a message
    SUSTAIN = "Sustain"  # SUSTAIN a REPORT
    VIEW = "View"  # Record a message VIEW

    # NOTIFICATIONS
    LIKED = "Liked"  # Notify when LIKED
    MENTIONED = "Mentioned"  # Notify when MENTIONED
    REPLIED = "Replied"  # Notify when REPLIED
    ADDED = "Added"  # Notify when ADDED
    REPORTED = "Reported"  # Notify when REPORTED

    message = models.ForeignKey(
        Message, on_delete=models.CASCADE, blank=True, null=True
    )
    action_by = models.ForeignKey(
        Contact,
        related_name="action_by_contact",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    action_for = models.ForeignKey(
        Contact,
        related_name="action_for_contact",
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    action_name = models.CharField(
        max_length=32, default=LIKED, blank=True, null=True
    )
    dismissed = models.BooleanField(default=False, blank=False, null=False)

    objects = MessageActionManager()

    class Meta:
        ordering = ["created", "pk"]
        verbose_name = "Action"
        verbose_name_plural = "Actions"

    def __str__(self):
        return "{} {} {}".format(
            self.action_by, self.action_name, self.action_for
        )

    # def get_absolute_url(self):
    #     return reverse("conversation.list", args=[self.message_to.pk])


reversion.register(MessageAction)
