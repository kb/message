# -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.contrib.auth import get_user_model
from django.db import transaction
from django.shortcuts import redirect, get_object_or_404
from django.urls import reverse
from django.views.generic import (
    CreateView,
    View,
    DetailView,
    ListView,
    TemplateView,
    RedirectView,
)
from rest_framework.exceptions import NotFound

from base.view_utils import BaseMixin
from contact.models import Contact
from .forms import MessageForm, NewMessageForm
from .serializers import MessageSerializer, ViewMessageSerializer
from .models import Message, MessageAction
from .service import search_between_two_contacts, search_lastofeach_per_contact
from .tasks import update_message_index


class ViewContactMixin:
    def get_request_contact(self, err):
        try:
            return Contact.objects.get(user=self.request.user)
        except Contact.DoesNotExist:
            # Create one as required.
            contact = Contact(user=self.request.user)
            contact.save()
            return contact


class MessageDetailMixin:  # (ContactPermMixin):

    model = Message

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.object.get_multi_as_dict())
        return context

    def test_message(self):
        return self.get_object()


class MessageListMixin:

    paginate_by = 10

    def get_queryset(self):
        qs = Message.objects.current()
        return qs.order_by("contact__user__username")


class AddMessageActionRedirectViewMixin(RedirectView):

    permanent = False
    query_string = True
    pattern_name = "reported.detail"
    add_actions = [MessageAction.LIKED]
    del_actions = [MessageAction.LIKED]

    def get_url(self, message, contact):
        return reverse(self.pattern_name, args=[message.pk])

    def get_redirect_url(self, *args, **kwargs):
        message = get_object_or_404(Message, pk=kwargs["pk"])
        contact = self.get_request_contact("No contact found.")
        for maction in self.del_actions:
            MessageAction.objects.del_message_action(
                message, contact, message.message_from, maction
            )
        for maction in self.add_actions:
            MessageAction.objects.add_message_action(
                message, contact, message.message_from, maction
            )
        return self.get_url(message, contact)


class MessageCreateViewMixin(ViewContactMixin, BaseMixin, CreateView):

    model = Message
    form_class = MessageForm

    def form_valid(self, form):
        with transaction.atomic():
            self.object = form.save(commit=False)
            to_username = form.cleaned_data.get("to_username", "")
            if to_username:
                message_to = Contact.objects.get(user__username=to_username)
            else:
                message_to = form.cleaned_data.get("message_to", None)
            self.object.message_to = message_to
            self.object.save()
            transaction.on_commit(
                lambda: update_message_index.delay(self.object.pk)
            )
        return super().form_valid(form)

    def get_initial(self, **kwargs):
        """Returns the initial data to use for forms on this view."""
        initial = super().get_initial()
        try:
            active_user = self.get_request_contact(err="No user")
            initial["message_from"] = active_user
        except NotFound as ex:
            pass  # Not true. This cannot happen.
        # See whether message is being added as part of an active conversation.
        if self.kwargs.get("pk"):
            message_to = Contact.objects.get(pk=self.kwargs.get("pk"))
            initial["message_to"] = message_to
        return initial

    def get_success_url(self):
        if self.object:
            return reverse(
                "conversation.list", args=[self.object.message_to.pk]
            )
        else:
            return reverse("conversation.list")


class ConversationsListView(LoginRequiredMixin, MessageCreateViewMixin):

    template_name = "message/conversation_list.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        criteria = self.request.GET.get("criteria", "")
        active_user = None
        try:
            active_user = self.get_request_contact(err="No user")
        except NotFound as ex:
            pass  # This wouldn't happen unless in a test.
        # Get all conversations for the requesting user.
        conversations = search_lastofeach_per_contact(
            criteria, active_user.pk, Message.MESSAGE_TYPE_PRIVATE
        )
        if self.kwargs.get("pk"):
            # Get the full conversation related to the given message.
            message_to = Contact.objects.get(pk=self.kwargs.get("pk"))
            conversation = search_between_two_contacts(
                criteria,
                active_user.pk,
                message_to.pk,
                Message.MESSAGE_TYPE_PRIVATE,
            )
            # Serialize conversation: Adds action summaries/contact statuses
            messages = MessageSerializer.setup_eager_loading(conversation)
            serializer = ViewMessageSerializer(
                context={"request": self.request}, instance=messages, many=True
            )
            context.update(conversation=serializer.data, conversee=message_to)
        context.update(conversations=conversations, active_user=active_user)
        return context


class NewConversationView(LoginRequiredMixin, MessageCreateViewMixin):

    form_class = NewMessageForm
    template_name = "message/message_new.html"


class MessageDetailView(
    LoginRequiredMixin, MessageDetailMixin, BaseMixin, DetailView
):
    template_name = "message/message_detail.html"


class ReportedDetailView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    MessageDetailMixin,
    BaseMixin,
    DetailView,
):
    template_name = "message/reported_detail.html"


class ReportedListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    MessageListMixin,
    BaseMixin,
    ListView,
):
    template_name = "message/reported_list.html"

    def get_queryset(self):
        mas = MessageAction.objects.current().filter(
            action_name=MessageAction.REPORT
        )
        exc = MessageAction.objects.current().filter(
            action_name__in=[MessageAction.OVERRULE, MessageAction.SUSTAIN]
        )
        qs = (
            Message.objects.all()
            .filter(id__in=set([ma.message.id for ma in mas]))
            .exclude(id__in=set([ma.message.id for ma in exc]))
        )
        return qs


class ReportView(
    LoginRequiredMixin,
    BaseMixin,
    ViewContactMixin,
    AddMessageActionRedirectViewMixin,
):
    pattern_name = "conversation.list"
    add_actions = [MessageAction.REPORT]
    del_actions = []

    def get_url(self, message, contact):
        conversee = message.message_to
        if message.message_to == contact:
            conversee = message.message_from
        return reverse(self.pattern_name, args=[conversee.pk])


class ReportedOveruledView(StaffuserRequiredMixin, ReportView):
    pattern_name = "reported.detail"
    add_actions = [MessageAction.OVERRULE]
    del_actions = [MessageAction.SUSTAIN]

    def get_url(self, message, contact):
        return reverse(self.pattern_name, args=[message.pk])

    def get_redirect_url(self, *args, **kwargs):
        message = get_object_or_404(Message, pk=kwargs["pk"])
        if message.deleted:
            message.undelete()
        return super().get_redirect_url(*args, **kwargs)


class ReportedPostponeView(ReportedOveruledView):
    pattern_name = "reported.list"
    add_actions = []
    del_actions = [MessageAction.OVERRULE, MessageAction.SUSTAIN]

    def get_url(self, message, contact):
        return reverse(self.pattern_name)


class ReportedSustainedView(StaffuserRequiredMixin, ReportView):
    pattern_name = "reported.detail"
    add_actions = [MessageAction.SUSTAIN]
    del_actions = [MessageAction.OVERRULE]

    def get_url(self, message, contact):
        return reverse(self.pattern_name, args=[message.pk])

    def get_redirect_url(self, *args, **kwargs):
        message = get_object_or_404(Message, pk=kwargs["pk"])
        contact = self.get_request_contact("No contact found.")
        message.set_deleted(contact.user)
        return super().get_redirect_url(*args, **kwargs)


class LikeConversationView(ReportView):
    pattern_name = "conversation.list"
    add_actions = [MessageAction.LIKED]
    del_actions = []

    def get_url(self, message, contact):
        conversee = message.message_to
        if message.message_to == contact:
            conversee = message.message_from
        return reverse(self.pattern_name, args=[conversee.pk])


class UnLikeConversationView(ReportView):
    add_actions = []
    del_actions = [MessageAction.LIKED]
