# -*- encoding: utf-8 -*-
from django.db import transaction
from rest_framework import viewsets, status
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from contact.models import Contact
from contact.views import SoftDeleteViewSet
from .models import Message, MessageAction
from .serializers import (
    LastOfEachSerializer,
    MessageSerializer,
    MessageActionSerializer,
    MessageActionSummarySerializer,
)
from message.service import (
    search_anyones_of_type,
    search_belonging_to,
    search_between_two_contacts,
    search_directed_to,
    search_lastofeach_per_contact,
)


use_eager_loading = True


class DismissActionMixin:
    def _flag_messages_as_viewed(self, qs, active_user, profile_id=None):
        """Flags messages as being viewed (on request)."""
        if profile_id:
            qs = qs.filter(message_to=active_user, message_from=profile_id)
        else:
            qs = qs.filter(message_to=active_user)
        message_ids = [m.id for m in qs]
        if message_ids and active_user:
            MessageAction.objects.filter(
                message_id__in=message_ids, action_for=active_user
            ).update(dismissed=True)


class ViewSetContactMixin:
    def get_request_contact(self, try_params=[], err="Not found."):
        """Returns contact based on a specified param/s, else request.user."""
        # TODO: Should try_param values be hashed or something?
        specified_id = 0
        # Try each param.
        for param in try_params:
            # Keep the first.
            specified_id = specified_id or self.request.query_params.get(
                param, 0
            )
        try:
            # If the param was specified...
            if int(specified_id):
                return Contact.objects.get(pk=specified_id)
            elif not str(self.request.user) == 'AnonymousUser':
                return Contact.objects.get(user=self.request.user)
            else:
                return None
        except Contact.DoesNotExist:
            raise NotFound(err)


class AnyonesViewSet(
    DismissActionMixin,
    SoftDeleteViewSet,
    ViewSetContactMixin,
    viewsets.ModelViewSet,
):
    """The entire active conversation between two users."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = MessageSerializer

    def get_queryset(self):
        active_user = self.get_request_contact(
            try_params=["curid"], err="User could not be found."
        )
        criteria = self.request.query_params.get("criteria")
        message_type = self.request.query_params.get("message_type")
        id = self.request.query_params.get("id")
        if id:
            qs = Message.objects.filter(pk=id)
        elif message_type:
            qs = search_anyones_of_type(criteria, message_type)
            self._flag_messages_as_viewed(qs, active_user)
        else:
            qs = Message.objects.current()
        if use_eager_loading:
            qs = MessageSerializer.setup_eager_loading(qs)
        return qs.order_by("-created")


class BelongingToViewSet(
    DismissActionMixin,
    SoftDeleteViewSet,
    ViewSetContactMixin,
    viewsets.ModelViewSet,
):
    """All the messages of a type belonging to the specified user."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = MessageSerializer

    def get_queryset(self):
        active_user = self.get_request_contact(
            try_params=["curid"], err="User could not be found."
        )
        criteria = self.request.query_params.get("criteria")
        message_type = self.request.query_params.get("message_type")
        if message_type:
            qs = search_belonging_to(criteria, active_user.pk, message_type)
            self._flag_messages_as_viewed(qs, active_user)
        else:
            qs = Message.objects.current()
        if use_eager_loading:
            qs = MessageSerializer.setup_eager_loading(qs)
        return qs.order_by("-created")


class BetweenTwoViewSet(
    DismissActionMixin,
    SoftDeleteViewSet,
    ViewSetContactMixin,
    viewsets.ModelViewSet,
):
    """The entire active "conversation" of a type between two users.

    By default messages are Message.MESSAGE_TYPE_PRIVATE message_type."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = MessageSerializer

    def get_queryset(self):
        active_user = self.get_request_contact(
            try_params=["curid"], err="User could not be found."
        )
        criteria = self.request.query_params.get("criteria")
        profile_id = self.request.query_params.get("profile_id")
        message_type = self.request.query_params.get("message_type")
        if profile_id and message_type:
            qs = search_between_two_contacts(
                criteria, active_user.pk, profile_id, message_type
            )
            self._flag_messages_as_viewed(qs, active_user, profile_id)
        else:
            qs = Message.objects.current()
        if use_eager_loading:
            qs = MessageSerializer.setup_eager_loading(qs)
        return qs


class DirectedToViewSet(
    DismissActionMixin,
    SoftDeleteViewSet,
    ViewSetContactMixin,
    viewsets.ModelViewSet,
):
    """All the messages of a type directed to the specified user."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = MessageSerializer

    def get_queryset(self):
        active_user = self.get_request_contact(
            try_params=["curid"], err="User could not be found."
        )
        directed_to_user = self.request.query_params.get("profile_id")
        criteria = self.request.query_params.get("criteria")
        message_type = self.request.query_params.get("message_type")
        if message_type:
            qs = search_directed_to(criteria, directed_to_user, message_type)
            self._flag_messages_as_viewed(qs, active_user)
        else:
            qs = Message.objects.current()
        if use_eager_loading:
            qs = MessageSerializer.setup_eager_loading(qs)
        return qs.order_by("-created")


# Don't use DismissActionMixin. Make the user click on the conversation
# to dismiss the unread statues.
class LastOfEachViewSet(ViewSetContactMixin, viewsets.ModelViewSet):
    """A readonly view of active conversations, returning only the very last
    message in that thread of a type."""

    authentication_classes = (
        TokenAuthentication,
    )  # TODO: Add "IsConnectedAuthentication class"
    permission_classes = (IsAuthenticated,)
    serializer_class = LastOfEachSerializer

    def get_queryset(self):
        active_user = self.get_request_contact(
            try_params=["curid"], err="User could not be found."
        )
        criteria = self.request.query_params.get("criteria")
        message_type = self.request.query_params.get("message_type")

        if message_type:
            qs = search_lastofeach_per_contact(
                criteria, active_user.pk, message_type
            )
        else:
            qs = Message.objects.current()
        if use_eager_loading:
            qs = MessageSerializer.setup_eager_loading(qs)
        return qs


class MessageActionViewSet(ViewSetContactMixin, viewsets.ModelViewSet):
    """Return the summary of message actions."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = MessageActionSerializer

    def get_queryset(self):
        requested_by = self.get_request_contact(
            try_params=["curid", "profile_id"], err="User could not be found."
        )
        action_name = self.request.query_params.get("action_name")
        profile_id = self.request.query_params.get("profile_id")
        curid = self.request.query_params.get("curid")
        if profile_id or curid:
            return MessageAction.objects.actions_for(requested_by, action_name)
        else:
            return MessageAction.objects.current()


class MessageActionSummaryViewSet(ViewSetContactMixin, viewsets.ViewSet):
    """Return the summary of a user's messages."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = MessageActionSummarySerializer

    def list(self, request):
        requested_by = self.get_request_contact(
            try_params=["curid"], err="User could not be found."
        )
        qry = []
        if requested_by:
            qry = MessageAction.objects.actions_for_summary(requested_by)
        return Response(qry)
