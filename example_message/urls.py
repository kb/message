# # -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.urls import path
from rest_framework import routers
from rest_framework.authtoken import views
from .views import DashView, HomeView, SettingsView
from contact.views import ContactViewSet
from message.api import (
    AnyonesViewSet,
    BelongingToViewSet,
    BetweenTwoViewSet,
    DirectedToViewSet,
    LastOfEachViewSet,
    MessageActionViewSet,
    MessageActionSummaryViewSet,
)

admin.autodiscover()

router = routers.DefaultRouter(trailing_slash=False)
router.register(r"walls", AnyonesViewSet, basename="walls")
router.register(r"contacts", ContactViewSet, basename="contacts")
router.register(r"mine", BelongingToViewSet, basename="mine")
router.register(r"messages", BetweenTwoViewSet, basename="messages")
router.register(r"tributes", DirectedToViewSet, basename="tributes")
router.register(r"conversations", LastOfEachViewSet, basename="conversations")
router.register(r"actions", MessageActionViewSet, basename="actions")
router.register(
    r"notifications", MessageActionSummaryViewSet, basename="notifications"
)

urlpatterns = [
    path("admin/", admin.site.urls),
    # DOES NOT USE A TRAILING SLASH
    # http://localhost:8000/api/0.1/message
    url(regex=r"^api/0.1/", view=include(router.urls)),
    # url(
    #     regex=r"^api/0.1/summary/$",
    #     view=MessageActionSummaryViewSet.as_view({"get": "list"}),
    #     name="message.summary",
    # ),
    url(regex=r"^", view=include("login.urls")),
    url(regex=r"^$", view=HomeView.as_view(), name="project.home"),
    url(regex=r"^contact/", view=include("contact.urls")),
    url(regex=r"^dash/$", view=DashView.as_view(), name="project.dash"),
    url(
        regex=r"^settings/$",
        view=SettingsView.as_view(),
        name="project.settings",
    ),
    url(regex=r"^message/", view=include("message.urls")),
    url(regex=r"^token/$", view=views.obtain_auth_token, name="api.token.auth"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        url(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
