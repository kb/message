# # -*- encoding: utf-8 -*-
from braces.views import LoginRequiredMixin, StaffuserRequiredMixin
from django.views.generic import DetailView, ListView, TemplateView

from base.view_utils import BaseMixin


class DashView(LoginRequiredMixin, TemplateView):
    template_name = "example/dash.html"


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "example/home.html"


class SettingsView(StaffuserRequiredMixin, TemplateView):
    template_name = "example/settings.html"
