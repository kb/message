# -*- encoding: utf-8 -*-
import os
import json
from dateutil.parser import parse
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from contact.models import Contact, Gender
from login.tests.scenario import (
    get_user_fred,
    get_user_mike,
    get_user_sara,
    get_user_staff,
    get_user_web,
)
from message.models import Message
from login.tests.factories import TEST_PASSWORD

dir_path = os.path.dirname(os.path.realpath(__file__))


class Command(BaseCommand):

    help = "Demo data for the message app"

    def _make_contact(self, fake_user, user):
        gender = Gender.objects.male
        if fake_user.get("gender", "") == "female":
            gender = Gender.objects.female
        contact = Contact(
            user=user,
            title=fake_user["title"],
            dob=parse(fake_user["date_of_birth"]),
            pk=fake_user["pk"],
        )
        contact.save()
        return contact

    def handle(self, *args, **options):
        data = []
        # users
        with open(os.path.join(dir_path, "users.json")) as f:
            data = json.load(f)
        self._make_contact(data[0], get_user_staff())
        self._make_contact(data[1], get_user_web())
        self._make_contact(data[2], get_user_fred())
        self._make_contact(data[3], get_user_mike())
        self._make_contact(data[4], get_user_sara())
        for fake_user in data[5:]:
            user = get_user_model()(
                username="{}{}".format(fake_user["last_name"], fake_user["pk"])
            )
            user.first_name = fake_user["first_name"]
            user.last_name = fake_user["last_name"]
            user.email = fake_user["last_name"]
            user.set_password(TEST_PASSWORD)
            user.save()
            self._make_contact(fake_user, user)

        # messages
        with open(os.path.join(dir_path, "messages.json")) as f:
            data = json.load(f)
        for fake_message in data[0:150]:
            message = Message(
                message_from_id=fake_message["message_from"],
                message_to_id=fake_message["message_to"],
                message=fake_message["message"],
                message_type=Message.MESSAGE_TYPE_PRIVATE,
            )
            message.created = parse(fake_message["created"])
            message.save()
        self.stdout.write("Demo data initialised")
