# -*- encoding: utf-8 -*-
from .base import *


# http://docs.celeryproject.org/en/2.5/django/unit-testing.html
CELERY_ALWAYS_EAGER = True

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "dev_message_tim",
        "USER": get_env_variable("DATABASE_USER"),
        "PASSWORD": "",
        "HOST": get_env_variable("DATABASE_HOST"),
        "PORT": "",
    }
}

# MIDDLEWARE += ("debug_toolbar.middleware.DebugToolbarMiddleware",)
#
# # force the debug toolbar to be displayed
# def show_toolbar(request):
#     return False
#
#
# INSTALLED_APPS += (
#     'django_extensions',
#     'debug_toolbar',
# )
# INTERNAL_IPS = ('127.0.0.1',)
# DEBUG_TOOLBAR_CONFIG = {
#     'INTERCEPT_REDIRECTS': False,
#     'ENABLE_STACKTRACES': True,
# }
