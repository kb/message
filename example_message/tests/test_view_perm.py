# -*- encoding: utf-8 -*-
import pytest

from django.contrib.auth import get_user_model
from django.urls import reverse
from contact.models import Contact
from login.tests.fixture import perm_check


@pytest.mark.django_db
def test_example_message_home_secured(perm_check):
    url = reverse("project.home")
    perm_check.auth(url)


@pytest.mark.django_db
def test_example_message_dash_secured(perm_check):
    url = reverse("project.dash")
    perm_check.auth(url)


@pytest.mark.django_db
def test_example_conversation_list_secured(perm_check):
    # # We need a contact to go with the authorized user
    # user = get_user_model().objects.get(username="staff")
    # Contact(user=user).save()
    url = reverse("conversation.list")
    perm_check.auth(url)


@pytest.mark.django_db
def test_example_message_new_secured(perm_check):
    # # We need a contact to go with the authorized user
    # user = get_user_model().objects.get(username="staff")
    # Contact(user=user).save()
    url = reverse("conversations.new")
    perm_check.auth(url)
