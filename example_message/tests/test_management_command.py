# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_example_message_demo_data():
    call_command("demo_data_login")
    call_command("demo_data_message")
