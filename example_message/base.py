# -*- encoding: utf-8 -*-
""" Django settings """
import os

from django.core.exceptions import ImproperlyConfigured
from django.urls import reverse_lazy


def get_env_variable(key):
    """
    Get the environment variable or return exception
    Copied from Django two scoops book
    """
    try:
        return os.environ[key]
    except KeyError:
        error_msg = "Set the {} env variable".format(key)
        print("ImproperlyConfigured: {}".format(error_msg))
        raise ImproperlyConfigured(error_msg)


DEBUG = True
TESTING = False

SESSION_COOKIE_SECURE = False
CSRF_COOKIE_SECURE = False

ADMINS = (("admin", "code@pkimber.net"),)

MANAGERS = ADMINS

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = "Europe/London"

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-gb"

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = "media"

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = "/media/"

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = "web_static/"

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = "/static/"

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = "w@t8%tdwyi-n$u_s#4_+cwnq&6)1n)l3p-qe(ziala0j^vo12d"

MIDDLEWARE = (
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "reversion.middleware.RevisionMiddleware",
)

ROOT_URLCONF = "example_message.urls"

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = "example_message.wsgi.application"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.request",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
            ],
            "string_if_invalid": "**** INVALID EXPRESSION: %s ****",
        },
    }
]

DJANGO_APPS = (
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    # Uncomment the next line to enable the admin:
    "django.contrib.admin",
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

THIRD_PARTY_APPS = (
    "rest_framework",
    # http://www.django-rest-framework.org/api-guide/authentication#tokenauthentication
    "rest_framework.authtoken",
    "reversion",
    "taggit",
)

LOCAL_APPS = (
    "api",
    "base",
    "block",
    "contact",
    "gallery",
    "message",
    "example_message",
    "login",
    "mail",
    "search",
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# http://django-rest-framework-json-api.readthedocs.io/en/stable/usage.html
# REST_FRAMEWORK = {
#     "DEFAULT_AUTHENTICATION_CLASSES": (
#         "rest_framework.authentication.TokenAuthentication",
#         "rest_framework.authentication.SessionAuthentication",
#     ),
#     "PAGE_SIZE": 10,
#     "EXCEPTION_HANDLER": "rest_framework_json_api.exceptions.exception_handler",
#     "DEFAULT_PAGINATION_CLASS": "rest_framework_json_api.pagination.PageNumberPagination",
#     "DEFAULT_PARSER_CLASSES": (
#         "rest_framework_json_api.parsers.JSONParser",
#         "rest_framework.parsers.FormParser",
#         "rest_framework.parsers.MultiPartParser",
#     ),
#     "DEFAULT_RENDERER_CLASSES": (
#         "rest_framework_json_api.renderers.JSONRenderer",
#         # If you're performance testing, you will want to use the browseable API
#         # without forms, as the forms can generate their own queries.
#         # If performance testing, enable:
#         # 'example.utils.BrowsableAPIRendererWithoutForms',
#         # Otherwise, to play around with the browseable API, enable:
#         "rest_framework.renderers.BrowsableAPIRenderer",
#     ),
#     "DEFAULT_METADATA_CLASS": "rest_framework_json_api.metadata.JSONAPIMetadata",
# }
# JSON_API_FORMAT_KEYS = "dasherize"

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug_false": {"()": "django.utils.log.RequireDebugFalse"}
    },
    "handlers": {
        "mail_admins": {
            "level": "ERROR",
            "filters": ["require_debug_false"],
            "class": "django.utils.log.AdminEmailHandler",
        }
    },
    "loggers": {
        "django.request": {
            "handlers": ["mail_admins"],
            "level": "ERROR",
            "propagate": True,
        }
    },
}

# URL where requests are redirected after login when the contrib.auth.login
# view gets no next parameter.
LOGIN_REDIRECT_URL = reverse_lazy("project.dash")

CONTACT_MODEL = "message.Contact"

DOMAIN = "test.kbsoftware.co.uk"
ELASTICSEARCH_HOST = "localhost"
ELASTICSEARCH_PORT = 9200

# https://github.com/johnsensible/django-sendfile
SENDFILE_BACKEND = "sendfile.backends.development"
SENDFILE_ROOT = "media-private"

FTP_STATIC_DIR = None
FTP_STATIC_URL = None

# `Message`.`message_type`s which require a MessageAction to be automatically
# created for newly saved messages for the recipient of `message_to` or when
# @mentioned + the required Action Name.
AUTO_ACTION_MESSAGE_TYPES = {"Private": "Replied"}

# For each message serialised, we list all the `MessageAction`s `actioned_by`
# the user requesting the list AND optionally those `actioned_for` them by
# another user if they are of a certain type.
INCLUDE_ACTION_TYPES_WHEN_FOR = ("Replied",)

# Action types which the user should be notified about.
NOTIFIABLE_ACTIONS = ("Liked", "Replied",)
