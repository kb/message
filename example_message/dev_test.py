# -*- encoding: utf-8 -*-
from .base import *


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "message_app",
        "USER": get_env_variable("DATABASE_USER"),
        "PASSWORD": "",
        "HOST": get_env_variable("DATABASE_HOST"),
        "PORT": "",
    }
}
