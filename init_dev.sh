#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

# py.test -x
DB_NAME="app_message_`id -nu`"
psql -X -U postgres -c "DROP DATABASE IF EXISTS ${DB_NAME};"
psql -X -U postgres -c "CREATE DATABASE ${DB_NAME} TEMPLATE=template0 ENCODING='utf-8';"

django-admin.py migrate --noinput
django-admin.py demo_data_login
django-admin.py init_app_message
django-admin.py demo_data_message
django-admin.py rebuild_message_index
django-admin.py rebuild_contact_index
django-admin.py runserver
